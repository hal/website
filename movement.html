<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Movement
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="movement">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Movement
      </h1>
    </div>
    <div id="content">
      <p>
        Imagine two people sitting by a pond. The pond where they are resting is in the middle of a lightly wooded
        area. They set themselves a minor challenge by agreeing to race back through the wood avoiding the paths. What
        have they agreed to do?!!!
      </p>
      <p>
        ….……
      </p>
      <p>
        Existentially our body moves in infinite space and physically encounters gravity and other objects. These
        objects have an effect on the body which we register through touch in all its various forms. The fundamental
        object is the ground beneath our feet; the point at which the effect of gravity is stopped. But, you say, we
        can <strong>see</strong> where we are going and all kinds of things around us. We are not moving blind. When
        someone is blind we can see that they live in a world where physical movement is cautious, careful and slow and
        frequently dependent on following large objects; such as wall surfaces; using touch. Special adaptations are
        needed for the simplest tasks.
      </p>
      <p>
        Familiar and intimate spaces can reveal that those who have never had sight can develop remarkable abilities to
        model space using movement patterns that draw on gravity, space and objects encountered by touch. We call this
        kinaesthesis. You can do something like this if you close your eyes but you would be using an integrated
        movement model. You would use both visual and movement modeling. You would have lost only current sight. Try
        and imagine being blind at birth. It is possible to have only a kinaesthetic model of a familiar space. It
        would be familiar because you had moved in it before and built a model of it. <em>Carefully reach out to grasp
        the handle of the mug that you keep in roughly the same space, and can tell largely where it is by the noise of
        your feet on the floor at the point under the table where you keep it.</em> As illustrated here, hearing is a
        distant sense that also gives information to the movement models and bears on their modification and
        triggering.
      </p>
      <p>
        We vary in all things and some people have better kinaesthesis and therefore movement accuracy, complexity and
        speed than others. One of the easiest places to see this happening is when we are putting on clothes that
        button at the front, on the mid-line of our bodies. The left side of our body is controlled by the right
        hemisphere of our brain and the right side by the left hemisphere. The control areas meet at the central line
        of our bodies. How well young children do up the buttons on their coats is a good indication of their natural
        co-ordination. Doing the buttons up without looking down at your hands and coat indicates that you are using
        only kinaesthetic and touch information to direct movement models; you are ‘feeling’ how to do it. If you look,
        then you are using visual information to help to control the movement models. Watch adults doing the same
        activity and you will see differences in how much they look down. <em>Clearly some coats are more difficult to
        do up then others.</em>
      </p>
      <p>
        I once bought an ice cream for a five year old. They dropped it and caught it in the same hand before it hit
        the ground. This was controlled by kinaesthesis not vision. Sadly, by then, gravity had changed the position of
        the ice cream in space and it was upside down. His daughter, at eleven months, was observed putting a round
        disc into a purpose made and exact circular indent in a board, by holding a knob fixed at its centre. She
        practiced this, looking while she did it. On one occasion she was seen looking away and carrying out the
        movement repeatedly. The precision of her kinaesthetic/movement model of space was remarkable. Try doing it
        yourself – no sliding allowed.
      </p>
      <p>
        The person born blind operating successfully in a familiar space behaves as if they can see providing the space
        remains unaltered. They are following a three dimensional and complex model of the space that has been
        constructed in their brain. You will be visualising this now and thereby missing the key point. This model
        cannot be visualised. It is built on movement distances, trajectories and tactile object knowledge set in space
        and gravity.
      </p>
      <p>
        Object knowledge requires touch as well as movement distance and trajectories in order to establish accurate
        size and shape. The objects in the space have to be at fixed points and the extent and shape of them will be
        duplicated in the model. The blind person can accommodate such as the mug being put down at a different place
        on the table and indeed the table itself being moved. They need to move around and touch in order to update the
        model in their head and reprogram the movement memories affected by the changes. Well practiced movements;
        motor models; may have become so habitual/automatic that they are difficult to alter and/or the update of the
        model may not be secure or complete. The blind person will then forget where they put the mug or knock it over
        or hit their leg on the table.
      </p>
      <p>
        The kinaesthetic and touch model of a given space is older in evolutionary terms than the contribution from
        vision. Taken together with visual spatial modeling, it is one of the bases of mathematics. See the <a href=
        "variation_unpredictability_and_mathematics.html">variability, unpredictability and mathematics</a> topic.
      </p>
      <p>
        This is such an important area let’s look at it from another angle. The core of the kinaesthetic brain model is
        the gravitational field set within empty space around our physical bodies. This includes the full distance that
        we can reach or extend our bodies. Animals vary in how much the kinaesthetic movement models are in place when
        they are born and how much time they take to mobilise them. We have the greatest potential for these models
        (remember the hands) but we are born the most limited and we take years to develop them.
      </p>
      <p>
        We grow these models gradually and with considerable development and learning experience. Spend three months
        with a baby starting when it is four months old and you will get the clearest feeling for what this model is
        and how it begins to be evident. <em>Contrast them with a blue tit using its small brain to hold a sunflower
        seed between its feet while stabbing with its beak to reach the heart of the seed.</em> The modeling emerges in
        phases from the brain and develops in interaction with real world practice and exploration. It is the steady
        deep properties of the physical world, together with our human emergent brain, that makes for common physical
        experience. Babies who are physically restricted catch up easily showing that a brain model is developing
        partially independently of movement. The muscles provide the force for movement but often for babies this can
        include causing pain by pulling your toe too hard or hitting yourself in the eye as you throw your arm around.
      </p>
      <p>
        The muscle movements become ‘coordinated’ and measured because the brain is building models of the space that
        includes the parts of the body as objects in space. In other words the first objects in the model are bits of
        our body captured in relation to one another; and not just at rest standing with arms dropped by our sides but
        in innumerable variations which go on developing throughout our childhood. It is so hard to avoid thinking of
        this modeling in anything but simple and visual terms but it is not at all simple and neither is it visual.
        <strong>When a baby reaches out for a distant object they are <em class="underline">then</em> moving in a
        visual model.</strong> The movement is recorded in the movement model but it has been triggered by the
        beginnings of a visual model of space that is itself biological unfolding within an experienced environment.
      </p>
      <p>
        The fact that these two ways of modeling integrate together; and we get the feeling and confidence that the
        world that we are moving in is the same world that we are seeing; is an example of biological coherence beyond
        which none of us can go. <strong>In philosophical terms it is therefore an a priori fact that the world that we
        see is the world that we move in.</strong>
      </p>
      <p>
        ….……
      </p>
      <p>
        Let us return to our two persons patiently waiting for something to happen. They wisely decide not to run
        through the wood with their eyes closed. Bouncing off trees, tripping over fallen branches, spraining an ankle
        in a small ditch and getting scratched by brambles holds no attraction for them. They now use what they first
        learned as small babies when reaching out for objects. There are objects out there that you can keep looking at
        until you come close enough to make physical contact with them or, as in this case, you can keep looking at
        them in order to avoid contact. In addition your unfocused peripheral vision is giving your brain potential
        objects. All your vision, including colour, is used in spatial modeling.
      </p>
      <p>
        Interestingly, knowledge derived from reading or talking is difficult to use, whereas previous experience of
        physically being in woods is crucial (even for the effective use of relevant word encounters). Seeing pictures
        or films about woods might help if those watching are accessing specific movement options (models in the brain)
        at the point where the picture or film is being viewed. On entering the wood they may have been told about
        being careful of tripping over fallen branches and misjudging footfall because of small ditches but if they do
        not have bodily experience of this wood or, less helpfully, woods like it, then the telling will only have thin
        meaning.
      </p>
      <p>
        Our persons first have to run down the steep bank away from the pond. The small babies reaching out to grab
        rattles hopelessly out of reach, have now developed and integrated the visual and motor models so well that
        they run down the bank glancing or looking down and placing their feet according to the slope and visual detail
        of the ground. Have you ever misjudged the distance a foot needs to go down and experienced either a hard
        physical impact on your body or loss of balance? In the first case the visual modeling has over identified the
        distance and in the second under identified it. In both cases you may have not looked enough or made a poor
        model. Perhaps someone spoke to you grabbing your focussed attention away from vision or maybe you looked away
        because something flew close to your eye or made a loud noise in your left ear.
      </p>
      <p>
        As person one and person two run through the wood their vision does not just have to model the immediate
        environment so that they step, jump, duck, twist and turn at the right places in physical reality. It has to
        model the environment as far as can be seen with the naked eye. Their eyes will be everywhere looking at
        patterns of physical light and turning them into the visual world. They will be working out and setting the
        route ahead and selecting appropriate movement models.
      </p>
      <p>
        This run is not just a physical act as they have agreed to do it together. It is a social act and though they
        have not agreed to go in the same direction they almost certainly will. Indeed a common scenario would be for
        the fittest and boldest of them to run ahead with their companion running in their steps {except for
        adjustments made primarily due to differences in size of gait, movement skills and emotional context
        (confidence}). As they do this, a path is born.
      </p>
      <p>
        Paths are used by nearly all ground animals despite the risks. There are many reasons for paths but secure
        physical movement is the most fundamental. Think of the object-self propelling in an unmodeled physical space.
        The animal following a path maker is following a proven route. Assuming person one is the path maker, person
        two would stop if person one fell down a hole and duck if they hit a branch. In the case of any animal,
        including humans, by re-using a path they have visual/movement modeling from previous experience. Even to use a
        route for the second time is enough to allow a great deal of re-cognition. The more that you use the path the
        more detailed the integrated visual and movement models become. This correspondingly decreases the demands on
        focussed attention. In the case of humans this creates yet one more opportunity for focussed attention to be
        captured by inner dialogue or dialogue with companions.
      </p>
      <p>
        In the man made environment that we have built we have created everything for the simplest and easiest of
        movements. Walking surfaces are flat and significant changes of elevation are facilitated using a serious of
        equal drops designed to correspond to simple and early learned models. <em>If the path created by person one
        and person two becomes popular, steps designed for humans will be built down the slope.</em> This build allows
        us to minimise the use of our focussed attention for movement and instead look at shop windows, dialogue with
        others, listen through earphones or disappear into our own minds and brains. Vision is needed to monitor other
        objects in the environment from things on the floor to the movements of other people and their wheeled objects
        but it only intermittently calls on focussed attention. But we can be caught out. There is a well-known video
        clip of crowds of people coming up the steps of a subway exit on to the pavement. Every so often one of them
        trips and looks around, and down at the steps, puzzled. All the steps are the same height except for one which
        is a half a centimeter higher. Interestingly it is likely to be those with the most accurate movement models
        who trip; assuming that they have clean functioning muscles. Those with poorer models will be looking down at
        their feet and integrating more visual information.
      </p>
      <p>
        The most remarkable example of what integrated visual and movement models can achieve is demonstrated by the
        professional snooker player. I do not know of a clearer place to see the <em class="underline">origins</em> of
        mathematics.
      </p>
      <p>
        In snooker the point of contact with the physical world is reduced down to the tip of a purpose made perfectly
        straight stick. This is ‘pushed’ into a small area on the back of a white ball. The exact spot within this area
        that it hits is chosen by the person pushing the stick. They can change the angle of the stick but more
        importantly the force that they use. The ball is set in a standard environment that is designed so that the
        chosen spot and the force used determine everything that happens as it moves. The ‘ground’ is a fine cloth over
        perfectly flat slate set within boundaries from which the ball bounces evenly. On this surface there are other
        balls of various colours and holes in the boundary. According to where and with what force the stick hits the
        white ball it will move, hitting one or more balls and/or the boundary. It may cause another ball to drop into
        a hole or drop in itself. The person holding the stick has to set up an integrated movement model in his head
        of where he wants the white ball to go and what he wants it to do. He then hits the white ball with the stick
        and the model in his head actually appears in reality as the contact, bounces, angles, travel distances and
        hole-drops he has modeled actually become physical events. To become professional at bringing these models to
        life, takes natural ability and vast amounts of practise as well as the ability to cope with social and
        emotional stress. The only way to fully grasp this human experience is to have a go on a full sized snooker
        table and then watch Ronnie O’Sullivan’s quarter final match in the 2014 Masters.
      </p>
      <p>
        In order to achieve what they do, the professional snooker player pushes the stick from behind as his chin
        rests lightly on it. Thereby, the physical distance between his eyes; and hence the centre of his binocular
        vision; and the distance between this point and the stick are maintained at the same ‘exact’ place. This is the
        tightest facilitated integration of visual and movement modeling that I am aware of.
      </p>
      <p>
        Of course, the professional snooker players are not expressing mathematics. Mathematics is stable and
        predictable and the movements of the players are not. They are using throughout <a href=
        "life_modeling.html">life modeling</a>. A lot of biological registers need to be functioning appropriately. At
        one time they would have a beer in their corner now it is sips from a glass of water. They practise endlessly
        and eat carefully. Their own emotions need to be balanced out in a model that works best for them in a match
        (emotions are a direct player in movement) and they must have ways of handling their emotions during the match.
        For this, helpful inner dialogues and intimate support from friends is needed. Then there is the preparation
        for, and the taking of a shot. The trigger of the shot is set in the condition and interactive modeling of the
        brain. This has to be precisely related to many muscles and to very complex integrated movement models that
        have behind them rich memory records that include episodic content - ‘the last time I played this shot in this
        venue I missed it’. You might be able to take this out of the inner dialogue (avoid thinking of it) but you
        cannot take it out of episodic memory itself. Your body still knows that you missed. Clearly, thinking about it
        would make it much harder to trigger the intended shot.
      </p>
      <p>
        <strong>Copying</strong>
      </p>
      <p>
        Many animals learn by copying the actions of others in their group. There is anecdotal evidence that after
        about six months old, babies begin a process of picking out for attention the other babies and children in
        social gatherings. They are fascinated by what they are doing and, given a choice, will concentrate on those
        just a little bit older than themselves.
      </p>
      <p>
        <em class="underline">In order to learn by copying you would ‘appear’ to have to, somehow, put yourself in the
        position of the animal that is doing the act that you are witnessing.</em> This raises the wild, ballooned,
        lazy philosophical nightmare of projection. It is made possible and tempting because of our shared language and
        therefor our shared thought - at least in the semantic rather than meaning cluster sense. There is nothing that
        can put you into the space and life of another human or animal. By all means listen, watch, sympathise,
        imagine, think, remember and learn.
      </p>
      <p>
        <em class="underline">Copying is using the integrated movement models that you have got, to attempt to do what
        you have seen happen in your <a href="episodic_memory.html">episodic memory</a> and, of course, in your visual
        spatial memory.</em> You have to have memory before having anything to copy. That is why babies eventually
        begin to pay attention to babies a bit older than themselves. They just might have the integrated movement
        models to do what they <em class="underline">have seen</em> i.e remembered.
      </p>
      <p>
        Animals that copy will generally be young and they will often restrict their copying not just to members of
        their species but to other youngsters, their parents or members of their group. They will also copy when they
        have the physical ability; gross (large muscle) motor co-ordination and strength; to attempt the action. Before
        this they will be looking.
      </p>
      <p>
        Remembering that we all vary biologically in everything, some babies; hence also children and adults; are
        better coordinated than others. Their movement models are more precise and better integrated with their visual
        models. The resultant integrated models are sharper, more accurate, flexible and robust than other babies of
        their age. What is important is that these babies learn to copy actions quicker than those babies with poorer
        integrated models. When you see babies and children who have poorer models, copying actions, they often attempt
        movements that they have not seen in the action that they are trying to copy. This suggests that their visual
        modeling is in service to their movement modeling. It is the integrated modeling of visual and movement
        perception that makes it possible to copy the actions of someone else. We see what they are doing, retrieve and
        adapt comparable models and attempt to produce the same action.
      </p>
      <p>
        As we continue living another important factor comes into play. We develop an increasing and ever more complex
        store of integrated movement models. <em>At given points these can also include useful stick-ons from sound,
        taste, touch or any other biological register (usually pulled together by episodic memory).</em> These models
        begin to include most of the physical situations that we are likely to encounter with the exception of:
        unmastered sports and hobbies; special physical arrangements needed with unfamiliar machines, old technologies
        and interactions with new or rare technology; and, increasingly, those models needed to function and stay alive
        in the natural environment. We largely live in a man made environment with only sanitised access to the natural
        world.
      </p>
      <p>
        Let’s assume that a four year old child called Snip is copying his friend Bam, who is building a bridge with
        large blocks. Snip’s brain has visual spatial modeling that locates the, not quite exact but within
        millimeters, position that Bam is in: Snip’s body having stood there numerous times before as well as having
        been in comparable places. Similarly it can locate Bam’s position in relation to the small table. From there it
        has models for handling the bricks (<em>the body having played with them, or ones like them before</em>), all
        Bam’s body positions and limb movements (<em>Snip’s being pretty well the same as Bam’s</em>), how the bricks
        fall when they are knocked down and much more. Snip’s brain has so far missed registering how still Bam is when
        placing the final brick over the void. As with snooker, fine refinements in models and their application to the
        physical world takes practise as well as copying.
      </p>
      <p>
        In a different scenario, Snip has entered the nursery for the first time and is a socially confident child.
        Having never before played with bricks and not knowing Bam, Snip goes over to the table and starts putting them
        on top of one another. Snip has already enough integrated modeling of space from both vision and movement to
        take part in the situation. The modeling we do is always work in progress and it is often approximate and
        poorly specified.
      </p>
      <p>
        ….……
      </p>
      <p>
        As we have seen, human beings develop integrated movement models in a process where emergent biological
        modeling is exposed to encounter experience. This maximises the complexity and flexibility of the modeling. It
        takes a lot of brain power. Animals with less brain capacity have simpler biological models with less capacity
        to develop. A pigeon sees and pecks at anything that meets general conditions. These conditions are largely
        about the size of the object within the immediate visual field. The model has a general and simple set of
        visual conditions combined with the movement of pecking any encountered example. It is a fully integrated
        movement model. <em>The pigeon relies on touch conditions in its beak to trigger the movements for swallowing
        or releasing.</em>
      </p>
      <p>
        A Meer Kat group, when out of its communal burrow, ensures that at least one of them is squatting on a high
        point looking at the sky. Knowledge of their lives reveals that in evolutionary terms their main predators have
        been birds of prey and that they now have built into their brains a model that links bird shapes in the sky
        with a vocal act and a rapid movement into the burrow. The sound of the vocal act then triggers the other Meer
        Kats to return to the burrow. Though there are many zoos where you can see Meer Kats looking at the sky it is
        rare to hear the warning call and witness the mass retreat. You are most likely to see this at Chessington Zoo.
        This is not because there are lots of birds of prey around but because there lots of aeroplanes high in the sky
        on their way to and from Heathrow Airport. Though we can discern the difference between aeroplanes and birds of
        prey the Meer Kats can’t. The visual elements of the integrated model that are built into their brains are too
        generally specified with limited flexibility (learning capacity).
      </p>
      <p>
        Social animals are strongly placed to use copying providing that they have the brain capacity. The shared
        modeling (<em>in the <a href="introduction_to_topic_language_and_shared_attention.html">introduction</a></em>)
        of Meer Kats is simple and balances three inherited brain models - looking for food, looking after the young
        and climbing to look for big birds in the sky. We are an animal that can not just model movements and actions
        incidentally, we can show other humans how to do things. The way in which we do this is complicated by speech
        and language. We look at this in the movement and language topic.
      </p>
      <p>
        Chimpanzees can also show each other how to do things and here there are no complications from language and
        thought - see the definition of thought we have used on this website. <a href=
        "introduction_to_topic_language_and_shared_attention.html">Introduction</a>. Showing requires the same
        conditions as copying together with social modeling of ‘teacher’ and ‘learner’. The social situation speeds up
        the learner chimpanzee’s ability to: find and use anvil and hammer stones to crack nuts and how to place and
        hit the nuts; how to select branches to wedge together, and sticks for different stages of building the night
        time tree nest; how to choose the best blade of grass and how to poke it far enough into the ants nest to
        capture ants; how to choose the best bit of moss to soak up the water trapped at the bottom of a hollow tree
        branch, preparatory to holding up the moss and squeezing the water into your mouth. There are other examples.
        Chimpanzees do not have language they are using their brains not minds.
      </p>
      <p>
        ….……
      </p>
      <p>
        What about the modern world where we have mechanical partners such as cars? The role of the driver is divided
        between simple kinaesthetic movements and rapidly changing visual spatial perception - unless on a quiet
        motorway. When driving, have you ever lost focussed attention (concentration) on the visual spatial modeling?
        This could be through thinking (inner dialogues) or talking with someone else or listening to music. We can
        find that we have missed turnings and have taken wrong turnings. When we lose focussed attention on a very
        familiar route we can realise twenty minutes later that we have just driven through busy traffic, made numerous
        manoeuvres and turns and ended up in the right place with no recall of actually driving? As a passenger, who
        can also drive, it is common to find that your own kinaesthetic driving actions and spatial movement modeling
        is operating alongside the drivers. Another example of integrated movement modeling. Often it is disturbing and
        uncomfortable as your modeling throws up actions and movements that the driver is not doing. <em>We usually
        assess these on the too risky to too cautious scale.</em> The driver is not driving with your brain modeling. I
        had one colleague whose ability to drive outside of focussed attention was remarkable. On many journeys he
        could include turning round to talk, extracting and passing round sandwiches and constant conversation.
        Needless to say the colleague was an excellent driver with split second responses.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="death_and_ritual.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="action_and_speech_and_language.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
