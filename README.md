This repo contains the HTML and CSS files for the [Human Animal Life](https://humananimal.life/) website.

These documents have been run through HTML Tidy using the `tidy.txt` config file and this command:

```bash
tidy -config tidy.txt -m *.html
```

The [Pure CSS 1.0.0](https://purecss.io/) files have simply been downloaded from [the URLs here](https://purecss.io/customize/#individual-modules).
