<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Introduction to topic language and shared attention
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="introduction_to_topic_language_and_shared_attention">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Introduction to topic language and shared attention
      </h1>
    </div>
    <div id="content">
      <p>
        What is going on here? :
      </p>
      <p>
        <em>I put lots of separate pieces of flexible objects into a container that has machinery attached to it. I
        then put in liquid chemicals that disperse in different combinations over a time period of around an hour. At
        times the temperature is raised. Various biochemical processes take place. I then use the machine to remove the
        chemicals. There are various ways of doing this but evaporation is the final material change that takes place.
        I then get the separate pieces of the flexible objects out of the container and carefully arrange them. I might
        decide to heat them up again but this time without any chemicals and using a weight to change their shape
        slightly.</em>
      </p>
      <p>
        This is a version of a training exercise designed to help teachers better understand the character of language.
        Many of you will not be able to work out what is going on. Some of you will get the right answer but not be
        sure that it is correct. Others will be able to argue that the description fits their wrong answer better than
        the correct answer. Indeed what is meant by right and wrong in this context? Is the right answer the ‘goings
        on’ that best fits the description or is it the ‘goings on’ that the person who wrote the description was
        thinking of as they wrote it. Toss a coin to decide. Descriptive language with careful choice of grammar and
        words can easily end up referring to nothing. It has all the right clothes but seems to dress a dummy not a
        body. In the above exercise we cannot go out and recognise or repeat the ‘goings on’ because we do not know how
        and in what way to engage with the physical and social world. We do not know the language game that is being
        played.
      </p>
      <p>
        <em class="underline">The language game we need is not in fact a language game at all.</em> Why, because what
        the above description lacks is special words that tell us what the social-physical ‘activity’ is. Words that
        tell us what we are writing or talking about. These words can come from any part of grammar and any sort of
        meaning cluster. They can be words that in some language pieces give a strong clue to ‘what we are talking
        about’ and in others help to hide ‘what we are talking about’. When viewed from the various modeling that makes
        up language, the way that we use these special words is random and undisciplined. <em class="underline">Their
        behaviour appears as random because we have not yet found the type of model that is using them.</em> We think
        that we are using some sort of language modeling because we are using words as content. <a href=
        "life_modeling.html">Life modeling</a> can use <em class="underline">anything</em> as content at any place or
        at any point in the expression of the model. The words that tell you what is ‘going on’ are words that take you
        to the shared focus of attention, the life model, that is being used. They take you to the real game that is
        being played.
      </p>
      <p>
        The key to participation in the human social world is to learn the games that are played and to develop the
        ability to identify them quickly and accurately. Lots of these games use language in various ways and in the
        absence of the games, language can also refer to them by choosing words that point to the game – the ‘goings
        on’. Our natural way of using language is heavily orientated to using words that indicate the game that is
        being played, so much so that it is hard to describe a ‘goings on’ without quickly pointing to it. In order to
        write the exercise above the words that point to what is going on have to be carefully removed. The language
        ability to refer/point to a life modeled game is called the topic. The meaning cluster (<a href=
        "meaning_and_grammar.html">Meaning and grammar</a>) behind the word ‘topic’ is vital to understanding how
        <a href="life_modeling.html">life modeling</a> integrates with language. It is odd to use language out of
        context without it relating to a topic. In the act of referring to ‘goings on’ using only language, topic words
        are vital. This is not a static process as topic shifts (foci of <a href=
        "attention_consciousness_and_self.html">attention</a> changes) are intrinsic to talk/communication.
      </p>
      <p>
        <em>The ‘goings on’ in the exercise above are the washing and drying of clothes, in a combined washing machine
        hot air drier, followed by ironing. Now you know this, the nature of the description has changed. Your <a href=
        "episodic_memory.html">episodic memory</a> will now generate still pictures, films, other sensory registers and
        feelings, and, if you let it, episodes and stories. You <em class="underline">cannot return</em> to your
        pre-topic condition.</em>
      </p>
      <p>
        Studying specific examples of dialogue and conversation by looking only at what is said gives a picture of
        constant breakdown and repair set in a litany of misunderstanding. Personal meaning clusters are part of the
        explanation for this but more fundamental is the fact that we are trying to study something that has been torn
        out of its natural context. What is its context? Consider the following: the physical and social context in
        which the conversation takes place and the way that each individual appears in the <a href=
        "episodic_memory.html">episodic memory</a> of the others (every aspect of a range of <a href=
        "biological_registers_and_existential_principles.html">biological registers</a>); the physical and social
        setting of the conversation and the episodic content energized during the conversation itself within each
        individual present (some of these finding their way into inner dialogues – thoughts - via semantics and word
        finding - <a href="meaning_and_grammar.html">meaning and grammar</a>); the <a href=
        "emotional_display.html">emotional display</a>, <a href="movement.html">movement</a> and body positions that
        accompany them and which are themselves able to change what is said and how it is expressed; the variation in
        intonation, emphasis and volume during the conversation and the direct and feedback effects of these; the
        beliefs of the participants (meaning all sentences and stories that the individual participants currently
        commit to) not forgetting that everything expressed in language and committed to is a belief – ‘I feed the old
        cat but I would rather see it dead’ – ‘I need new pegs’. (<a href="believing_and_knowing.html">believing and
        knowing</a>) <em>Some early studies into conversation were based on secret recordings made on the top of London
        Transport buses. From this strange source much was deduced about people’s language. Contrasts were made with
        students in seminars. Strangely no secret recordings were made of students on the top of buses.</em>
      </p>
      <p>
        Topic is important because it keeps the language being shared in touch with all the other elements of the
        <a href="life_modeling.html">Life modeling</a> that is being created in the social encounter. <em class=
        "underline">Only with topic monitoring can language function as part of the social world.</em>
      </p>
      <p>
        In the topics of <a href="speech_and_language.html">speech and language</a> and <a href=
        "meaning_and_grammar.html">meaning and grammar</a>, we look at the character of language itself but we must
        place it within the world of shared attention life modeling; watching the football match together on the
        television or at the pitch, the orchestra rehearsal, the football warm up, serving together in a busy cafe,
        clearing snow with the neighbours, the board meeting, team crane-assembly, taking the cat to the vet with your
        five year old, changing your mind about what you and your partner are going to have for a meal and engaging
        with social media as well as playing computer games.
      </p>
      <p>
        The last two raise the existence of what is best called social world two: the fact that your feelings and
        sensory activity as well as aspects of your social registers led by visual imagery, sound and language can
        become a major part of an advanced social world mediated and dependent on technology. Clearly social world two
        can integrate with much of the creative and flexible life modeling taking place in social world one. Its reach
        is extended across the whole planet allowing new and different <a href="intimacy.html">intimacy</a>. It is hard
        to imagine a human species social world without it. Often we shift back and forth from social world one to
        social world two. Picture a friendship group of fourteen year old’s all sitting together but all absorbed in
        attending to their own personal mobile phones where they are communicating with other people or, to a group of
        young men playing violent games on a computer when the house and occupants next door are destroyed by a
        missile. <em>This was a news event from Gaza.</em>
      </p>
      <p>
        <strong>Shared attention, life models and language games</strong>
      </p>
      <p>
        I have always had reservations about Wittgenstein’s referring to the use of words as ‘language games’. He was
        moving away from a ‘particular’ i.e. a mathematics style model account of meaning to one where words are
        defined by their use. Their use he called language games but this invites us to adopt a position where we treat
        language as a game and have the option of choosing not to play the game and stand on the side watching.
        Wittgenstein’s approach did allow him to explore how we think and talk and thus expose difficulties,
        contradictions and the dangers of being captured by our language. On the other hand to refer to language as
        games is a touch of genius. We only have to reword to <em class="underline">‘meaning is the use of words taken
        as the language part of social games’</em> to find ourselves playing a similar ‘game’ to Wittgenstein.
      </p>
      <p>
        The word ‘game’ can also stand in for life modeling, ‘you are just playing games with me’ but it still suggests
        that we can keep much of ourselves out of the games of social life and stand apart from our own <a href=
        "believing_and_knowing.html">beliefs</a>. <em>Is it true that if I leave my room and go downstairs I can find
        facilities for making tea? Was it true in the hotel mentioned elsewhere that if you left your towels on the
        floor they would be replaced? The first seems silly, as I am unlikely to find myself using language in this
        situation, but the second requires belief in order for me to act by leaving the towels on the floor.</em>
      </p>
      <p>
        Another problem with using the word ‘game’ is that it implies winners and losers and success and failure. These
        are significant parts of the meaning clusters that lie behind our use of the word game. The only way to address
        the problem of using the word ‘game’ is to talk of life modeling in the context of <a href=
        "intimacy.html">intimacy</a>. Picture a mother with a new-born baby demonstrating the beginnings of intimate
        life modeling.
      </p>
      <p>
        Language constitutes the mind, as distinct from the animal brain, through its ability to refer to experience.
        This creates the feeling of its separate existence and the fallacy of it having a life of its own.
      </p>
      <p>
        <em class="underline">In our animal nature whatever is in focused attention is our point of contact with both
        the social and physical worlds. In our use of language this is replaced by topic. We can illustrate this
        easily.</em>
      </p>
      <p>
        <em>Four people are watching a football match on the television. We take an audio trace and play it back to
        hear their conversation. We can hear the crowd sound in the background. There is very little actual
        conversation around a topic. There is constant language but it is not clear what it is referring to. We cannot
        identify the topics and there is barely any evidence of monitored/managed topic shift. A lot of words, phrases
        and sentences are expressions of feeling. Write your own examples. We can find more meaning in the following
        edited samples: ‘it was a dive’; ‘come on, come on pass it to x’; ‘where’s the ref and linesman, y was
        offside?’; ‘not again’, he never takes his chances’; ‘we need to buy a striker’. In thirty two examples of
        sentences like this only one led to a reply. None of the remainder became part of a conversation. Suddenly a
        conversation does break out. Sentences are spoken and the topic shift is clear. The topic moves from the need
        to change the manager and discussion of possible candidates to strikers that might be available on loan and
        whether the new owner is going to put his hand in his pocket and buy one. It is half time.</em>
      </p>
      <p>
        When the match is on, the shared focused attention is the shared experience of watching the match on the
        television. The focus of attention is the match. All the language appears as bits and pieces of shared
        attention. At one point there is an attempt to start a conversation about what to eat but it is ignored. During
        half time, a new but related shared focus of attention is held together by language through the use of topic
        words. The pattern of quick changes between topic and shared attention is typical of our social world.
        (<a href="intimacy_movement_and_shared_attention.html">intimacy, movement and shared attention</a>)
      </p>
      <p>
        Human interaction is often described as reciprocal and our relationship with each other as attachment. Both of
        these words are misleading and inappropriate. Their meaning clusters are rich with the sense of independent
        ‘objects’ exchanging and linking with one another. The truth is that we are intimately involved with one
        another in an expansive, biologically anticipated, shared experience world from the womb. <em class=
        "underline">We carry within our individual body/brain a combined individual and social nature.</em> We are
        damaged and fail to express our nature to the extent that we are unable to live our lives with each other while
        following a natural course. Isolation is the harshest non-violent punishment and is used from the naughty step
        to the punishment cell in prison. <em>We usually think that these are physical restrictions as we have to stay
        in a limited place but if we could move we would find others.</em> When we have developed inner dialogues, a
        rich episodic memory of intimacy and, vitally, a range of complex life models we can cope with isolation better
        but our capacity to create worlds is by then inexhaustible.
      </p>
      <p>
        The view of ourselves as, ‘individuals who then socialise’ is deeply embedded in the dominant culture that
        drives contemporary society. It penetrates all levels of our being. It distorts our understanding of everything
        even the picture of our own nature. The problem is that commitment to, belief in, the sentence ‘human beings
        are individual animals who then socialise’ is a mistake and leads to a ‘harvest of bad outcomes’. Of course all
        beliefs which are consistent with such a fundamental sentence lead to the same bad outcomes. If, in our own
        journey on this website, we were to continue to assume individual self-intact humans then we would have great
        difficulty in understanding our social biology and by infection the nature of human experience. In particular
        we would make a big mistake about the nature of language.
      </p>
      <p>
        Surely we can clearly be seen to be individuals who then socialise with one another? We look at this elsewhere
        when we establish that we can only sense the physical world and in this we are self-evidently individual. From
        this position we can only <em class="underline">see</em> - sense/be aware of - confused evidence of the social
        world which in turn only establishes that we take part in something else. This ‘taking part’ makes us choose
        words like ‘reciprocity’ and ‘attachment’ which are so visible that they can be used of physical and mechanical
        connection. We find it hard to believe that some small birds will feed their cousins babies or that young human
        babies begin life making complex movements that have no use or meaning in the physical world and which need a
        human partner to develop into the models that create the rich social world of humans, a world that can only be
        lived.
      </p>
      <p>
        <strong>Let’s take a look at the rich episodic memory of intimacy.</strong>
      </p>
      <p>
        <a href="episodic_memory.html">Episodic memory</a> is the source of the richness of our encounters both in the
        physical world and the social world. Without it we have very little to bring to any encounter.
      </p>
      <p>
        Though each <a href="biological_registers_and_existential_principles.html">biological register</a> has its own
        encounters every actual (switched on) <a href="movement.html">movement</a> or action (a group of movements) is
        accompanied by the condition of all registers that are high enough to make it into the episode, led by
        whichever one is occupying the focus of attention at the <strong>time</strong> – most commonly vision and inner
        dialogue (thinking).
      </p>
      <p>
        <em>Time in experience is not the same as material time. We know this well as we feel the speed and slowness of
        experienced time and are often surprised by its mismatch with material time. This is yet another pointer to the
        fact that mathematical modeling and life modeling (LM) are existentially completely different. Movements and
        actions take place over material time in the encounter itself but for each individual taking part experienced
        time and the participating registers come and go. The episodic register is not in any sense linear, flat,
        layered or multidimensional as in the great body of mathematics. It is held together by the complex shifts of
        co-incidental content from across the brain and mind, movements/actions and the foci of attention. Experienced
        time is a feeling that arises from: the amount and rate of change, the nature of the experience, its relation
        to immediately previous register activity and the ‘make-up’ of the individual.</em>
      </p>
      <p>
        Our episodic memory begins its work here but also pulls together models that we might describe as summaries and
        patterns. Experience builds across our brain and in every specific register. It is these experiences that then
        inform new foci of attention and help to prepare possible actions and movements. Much of our brain capacity is
        dedicated to episodic memory so we can experience as much as possible and prepare for future encounters. To
        make things simpler episodes are pulled together in scenarios or stories which explains our absolute
        fascination with stories.
      </p>
      <p>
        In the social and thinking (inner dialogue) contexts stories are not just in the telling. They can be
        ‘illustrated’ by images and sounds and indeed movements, postures and any other registers that recognisably
        appear in the episodic memory of those present. Indeed we can go further. Stories can be <em class=
        "underline">told</em> by other registers. We can move from ‘illustrated’ topics to shared attention life models
        made by the pictures themselves. Here the words become appendages (illustrations). Think of silent films,
        comics and You Tube. You can use sounds, especially music, as well as smells and facial expressions and
        posture/movement (mime) as the foci of attention for intimate shared attention life models in storytelling.
      </p>
      <p>
        Intimacy based shared attention life models are inexhaustible in their variation and are derived from a special
        type of <a href="movement.html">movement</a> model which is open to nearly all register content. In addition
        they can put elements together in a theoretically infinite number of ways. Casual ones animate our social life
        while culture for any group is defined by the those that are learned and taught by the group. These are
        developed within social groups and their nature is restricted by the importance of maintaining the shared world
        of the group. Indeed intimacy can be deepened by the extent to which they are played out in the same way with
        minimum variation. This can be so important to a social group that even when any original purpose in the
        physical world has been lost the models can still be played out solely for intimacy. Indeed the greatest
        intimacy can be achieved when the model is carried out without any reference to purpose in the physical world.
        It can become very fixed and highly demanding of repetitive accuracy. We call these rituals.
      </p>
      <p>
        Within ourselves, therefore, we do not just have inner dialogue/thought we also have models from all our
        experience selected from episodic memory and energised by emotion. It is the dynamism of these that fills our
        waking hours and allows us to adapt so fast to, what does become, a world of endless and extended encounter.
        Clearly this has to be backed up by the robustness of episodic memory in all its fullness and the multitude of
        biological registers that sustain it. With all of these, language races through with constant topic change –
        sometimes taking the lead and sometimes making fragmented contributions when it sits in the back seat.
      </p>
      <p>
        Any animal group capable of shared-attention will be using shared-attention life models. This applies to all
        monkeys and primates. Let’s see how the Meer Kats are getting on. How do the Meer Kats take turns looking out
        for danger and looking after the infants? How do they co-operate in defence of their territory? How do they
        ensure that each member of the group gets enough time to forage for food? We can talk about why things happen
        but what matters is how they happen. It is intimate shared-attention life models that stop all the Meer Kats in
        a group climbing bushes and looking for eagles at the same time. It stops them all simultaneously looking after
        the young and stops them all going foraging for food neglecting the young and not bothering to look for eagles.
      </p>
      <p>
        <em class="underline">Shared-attention works by using the foci of attention of those taking part.</em> Each
        animal participant is therefore creating an episodic memory for those bits where they are paying attention.
        This explains why ‘not paying attention’ is openly criticised in human affairs. One of the most satisfying
        experiences when meeting someone for the first time is the extent to which you can reference the same life
        models. These can be anything from pool, football, yoga, leisure shopping, choir, listening to music choices,
        to topics where conversations expand into the topic (quick topic changes often being a sign that intimacy is
        not developing as each topic runs into breakdown). The further that you follow compatible routes into a topic
        the stronger the feeling of intimacy. When this happens under the right circumstances, and when many other
        topics show the same pattern, intimacy grows and we begin to talk of friendship. Even stronger contributors to
        friendship are shared-attention movement life models (shared activities). More powerful still are instances
        where we appear in each others episodic memories in a way that draws in strong levels of the <a href=
        "emotions.html">Emotions</a>; shared threats, shared difficulties, shared loss, shared satisfaction and shared
        interest and the episodes and stories in which they are embedded. The latter can be very rich indeed
        particularly as we are biologically evolved to spend our lives largely with people we encounter between birth
        and adolescence, death being the only separator.
      </p>
      <p>
        <em>A stranger, in our original social world, would have been someone not known by anyone in our wider group
        (numbering somewhere between fifty and a hundred and fifty). Indeed it is very likely that it was a long time
        before the word ‘stranger’ appeared in any early language.</em>
      </p>
      <p>
        <em>The depth/extent of intimacy is ‘measured’ as the degree of ‘richness’ of the shared episodic memories of
        the individuals involved. They are, of course, not the same memories. They are compatible and similar and taken
        by the parties to be memories of the same event. Shared-attention life models such as physical activities and
        games alongside dialogues and stories help to keep <a href="intimacy.html">intimacy</a> ‘present’ and
        fresh.</em>
      </p>
      <p>
        Elsewhere we describe the way strong shared emotions build intimacy but strong emotions are also present in a
        conflicted way within encounters. If you meet someone for the first time and have high registers for
        apprehension/fear or irritation/anger – <em>note we have moved in our referring from the perceptual function of
        these emotions (threat and difficulty) to their biochemical messengers</em> – then the development of intimacy
        is prevented (possibly only temporally). Open avoidance or hostility can develop but more usually <a href=
        "emotional_display.html">emotional display</a> models prevent you from communicating this to them thus giving a
        chance for intimacy to develop without the need for repair. The richer the intimacy between you and someone
        else the more likely you both are to experience incidents of apprehension/fear and irritation/anger triggered
        by one another. In addition the display models are often not applied and are also more easily seen through so
        that you become more aware of what the other person is feeling. The ability of ‘relationships’ to absorb
        tension and conflict is vital to all social animals.
      </p>
      <p>
        All animals would prefer to only experience satisfaction/happy and interest/excitement. To expect <em class=
        "underline">only</em> these <a href="emotions.html">emotions</a> from physical and social life is to
        misunderstand the challenge of being alive.
      </p>
      <p>
        Even when an intimate shared-attention life model is clearly identified, and new human participants shown and
        told how to take part, the real life expression of it has infinite possibilities. Are any two football games
        the same or any two walks in the park or the same family group visiting the same Zoo a month later? The
        components all change enough to create a new experience even taking the same dog for a walk following the same
        route at the same material time every day and meeting the same people doing the same with their dogs. In life
        modeling there is no repetition as compared with mathematical modeling where it is fundamental.
      </p>
      <p>
        Animal species have intimate shared-attention life models at various levels of complexity but none of them have
        language. We have taken the view here that the word mind is better used only of language and thought. Other
        brain registers and modeling are very close to language and thought. So close that we can see where we switch
        between mind and brain as we move between topic-led Life Models and shared attention Life Models. We have
        already seen that when language is used as part of shared attention Life Models (watching the football match)
        it is fragmentary and without topic links, incoherent. To use the word mind to refer to what it going on in the
        body/brain of animals – <em>yes, this does include us</em> – reveals the extent to which we fail to understand
        how our brain/body actually functions.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="meaning_and_grammar.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="emotions.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
