<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Biological registers and existential principles
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="biological_registers_and_existential_principles">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Biological registers and existential principles
      </h1>
    </div>
    <div id="content">
      <p>
        In many topics on this site we find expressed the awareness of our embodiment. How do we understand and how do
        we explore our embodied nature? How do we focus on our individual bodies? <em>‘Only our bodies are the
        principle of individuation’</em> (Wittgenstein).
      </p>
      <p>
        We have largely walked away from believing that the individual continues after their body is dead.
        Unfortunately rejection of this cannot bring us back into our bodies. We are left with an existential fear. Our
        age is still full of cultural stories that explore the nightmares of having our body taken over by ‘something
        else’. We have the added possibility that we will be replaced by technical robots indistinguishable from us
        lacking only the ‘I’ in the machine or even more terrifyingly that they will develop an ‘I’ of their own.
      </p>
      <p>
        Our deep fears are based on reality. We are both, a vehicle for referring (thought and talk)
        <strong>and</strong> a body full of organs that we are largely unable to access by thinking. In other words we
        do not know our own bodies in general let alone moment by moment. I cannot tell my kidneys to speed up - though
        I can control my breathing as I need to do this to swim underwater and to talk. I do not know by what means I
        caught the ice cream cornet; that I dropped; before it hit the ground (<em>sadly, due to gravity, upside
        down</em>). I do not know if I am developing a cancer. I do not know if my blurred vision is muscle tension or
        the sign of some fault in my brain. I do not know if I am still able to climb over a five foot fence. The mind
        (thought and talk) is very limited in its <em class="underline">direct knowledge</em> of our embodied nature.
        <em>‘What belongs to the essence of the world cannot be expressed by language’.</em> (Wittgenstein)
      </p>
      <p>
        In the relation to the physical body the mind is inert semantically but very alive in the dark side of
        semantics, the meaning clusters, where there is the ‘excitement’ of <a href="memory.html">memory</a>, <a href=
        "emotional_display.html">emotions</a> and feelings. Meaning clusters are explained in the <a href=
        "meaning_and_grammar.html">Meaning and grammar</a> topic.
      </p>
      <p>
        In relation to the body that it finds itself in, what troubles the mind most is that there are other biological
        registers that have a powerful all body remit. Its understanding of these is so bad that it does not have words
        for the individual five basic emotions themselves. It only refers to them in the context of a degree of feeling
        such as in ‘annoyed’ and ‘angry’.
      </p>
      <p>
        <em class="underline">We need the existential principles to keep us firmly in our bodies trapped with our
        bundle of bones, muscles, blood and organs. Only then can we properly understand the biological registers.</em>
      </p>
      <p>
        <em>‘It doesn’t strike us at all when we look round us, move about in space, feel our own bodies, etc. etc.
        because there is nothing that contrasts with the form of the world’.</em> (Wittgenstein)
      </p>
      <p>
        <strong>The biological registers</strong>
      </p>
      <p>
        The biological registers are the bits of kit that you have been gifted by nature. Though interesting in
        themselves the most fascinating aspect is how many, very separate registers function within our body. In this
        website we keep coming across them according to what we make of them existentially rather than scientifically.
        Biological registers are very numerous and many are largely out of our awareness such as those involved in
        digestion, the immune system, brain maintenance (sleep), conception and pregnancy. Some that we find
        existentially vital such as touch and smell are far more complex than we are commonly aware of. In the website,
        we spend a lot of time with one register that is ‘out of view’, except in outcomes. It is fundamental to our
        bodily structure and movement but we take its natural environment as a given. The register is kinaesthesis and
        its home is gravity and space. It is the silent major sense.
      </p>
      <p>
        A biological register is something that has its own encounters with what is outside, and in some cases what is
        inside, the body. Something that has its own terms and conditions. Something that is one of the bundle of
        ‘elements’ that participates in our brain. The biological registers that make up speech and language make it
        possible for us to have both a mind and a shared mind with others of our species.
      </p>
      <p>
        The physical and material nature of our world, increasingly revealed and described by science, is implicit in
        all animal bodies: environmental temperature ranges, gravity and the contents of the air we breath (this is not
        to exclude animals that live at the bottom of the ocean in a methane environment). Science is constantly
        discovering new registers.
      </p>
      <p>
        All biological registers have been evolved. Numberless deaths and last minute, knife edge successful breedings
        have created them. They range from those that respond to characteristics such as gravity (balance); critical to
        how we move; to pheromones that we put within the meaning cluster of ‘smell’. They range from the complex
        aspects of ‘touch’; that defines our physical boundaries and is vital to our story of intimacy and social life;
        to our complex vision which dominates our understanding of the world around us (‘getting the picture’). <em>If
        you trip over this point, assuming that speech and language does this instead of vision, read example three on
        the <a href="action_and_speech_and_language.html">speech and language</a> topic.</em>
      </p>
      <p>
        <strong>If language and thought; the mind; does not resolve the conflicting biological registers and guide the
        whole animal then what does?</strong>
      </p>
      <p>
        We know that whole animal guidance is complex because of the difficulty in predicting it. This is particularly
        true when the animal is in its natural environment.
      </p>
      <p>
        If the mind does not guide our whole animal body then we would seem to be in the dark. With the careful use of
        a torch we can pull together a picture of how it is done.
      </p>
      <p>
        Firstly there appear to be four important ‘pinch points’ where conflicting biological registers and life models
        are resolved.
      </p>
      <p>
        The first is the movement trigger. Complex movement and action models can go on for some time. We examine this
        in the <a href="movement.html">movement</a> and action topic.
      </p>
      <p>
        The second is focussed attention. We look at this in the <a href=
        "attention_consciousness_and_self.html">attention, consciousness and self</a> topic.
      </p>
      <p>
        The third is shared attention. Explanation of this can be found in the <a href=
        "introduction_to_topic_language_and_shared_attention.html">introduction</a> and in the <a href=
        "intimacy_movement_and_shared_attention.html">intimacy, movement and shared attention</a> topic.
      </p>
      <p>
        The fourth is ‘topic’ which you can read about in the <a href=
        "introduction_to_topic_language_and_shared_attention.html">introduction</a>.
      </p>
      <p>
        All of these use the same basic form of a life model. These are highly flexible and in themselves as
        unpredictable as the encounters experienced by the embodied animal. We examine the proposition that due to
        their defiance of mathematics they are impossible to scientifically describe and capture in the <a href=
        "variation_unpredictability_and_mathematics.html">variation, unpredictability and mathematics</a> topic.
      </p>
      <p>
        <strong>What about the brain?</strong>
      </p>
      <p>
        The brain is a visible physical organ where many complex things happen. It is not an ‘it’ that does things.
      </p>
      <p>
        The animal brain is made up of many different biological registers. The human brain has the most evolved
        variation of these. There comes a point where animal brains become so complex that they need to partially shut
        down (experienced as sleep) in order to move out toxins created by functioning. This also allows for different
        life modeling to take place.
      </p>
      <p>
        Some primitive life forms, such as jelly fish, can lack a brain because their different biological registers
        still have their own relationship with what they encounter. The ongoing encounters, experienced by these
        registers, bring about the survival and reproduction of the life form. But, they <em class="underline">are</em>
        in the same body. The first step to developing a brain.
      </p>
      <p>
        In all animals there are numerous biological registers that have their own encounters without involving brain
        activity. Think of the complex functions of other organs such as the pancreas, kidneys and liver.
      </p>
      <p>
        Many jelly fish are moved by sea currents but some can move themselves. It is likely that a simple biological
        register for sea temperature or for light and dark triggers the movement. This the beginning of the
        relationship between animal biological registers and movement. Even in humans some biological registers can
        trigger movement without involving the brain. We call this a reflex. The sense of touch can do this. <a href=
        "movement.html">Movement</a>
      </p>
      <p>
        We think of the brain linking everything together, but from the perspective of natural philosophy we need to
        see it as containing many different organs and separately evolved biological registers experiencing their own
        encounters within and without the body. The interconnections between these elements are themselves separately
        evolved and distinct biological entities. No animal brain, including ours, has the brief to ensure the animals
        survival. There is no set up, biological or ethereal, which has an overview and energises or moves resources
        around based on a set of priorities. For example, focussed attention is not a conscious command centre but high
        ground where the currently strongest register stands.
      </p>
      <p>
        <strong>We are a living body but where are we and what do we encounter?</strong>
      </p>
      <p>
        The bodies of animals move. We explain in more detail how this happens in the <a href=
        "movement.html">movement</a> topic. We originally moved in endless water but we have evolved to have our
        embodied existence in space; in a mix of gases. Scientifically this space is finite. Existentially it is
        infinite, containing the presence of ‘objects’ which we register according to whatever kit we have been gifted
        with. <em>‘The space of human movement is infinite in the same way as time’</em> (Wittgenstein). Walking in the
        dark, or walking with your eyes closed, in an unknown, therefore existentially infinite space, will reveal to
        you your strong and natural feeling that there are objects around. You do not even have to move, as you will be
        standing or sitting on another object or suspended from something. You are depended on the registers that you
        have been gifted. One of the major ones, you have just disabled by closing your eyes or by the loss of light.
      </p>
      <p>
        Our lives are set within a ferociously complex and unknowable reality. Indeed there are three infinities, space
        time and complexity and they are all with us all of the time. We can see infinite complexity at its fullest in
        the shared mind of the social world. We live in a world of likeness and similarity energised by
        unpredictability. Not one of unambiguous singularity and identity energised by predictability.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="variation_unpredictability_and_mathematics.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="gender_and_temperament.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
