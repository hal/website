<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Believing and knowing
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="believing_and_knowing">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Believing and knowing
      </h1>
    </div>
    <div id="content">
      <p>
        It would be useful if you read the <a href="meaning_and_grammar.html">meaning and grammar</a> topic where we
        encounter two important topics. These are firstly the fundamental distinction between object and action and
        secondly the distinction between semantics and meaning clusters.
      </p>
      <p>
        The brain uses seeing in order to do three major things, firstly to give information to the ‘on the move’
        movement models, secondly to detect movement in the visual field and finally to identify objects (object
        perception). <strong>The separate homes of ‘the object’ and of action, lie in vision before they appear in
        language.</strong>
      </p>
      <p>
        If we look at these deep perceptions of ‘object’ and ‘action’ we see an interesting difference between
        believing and knowing. Both of these words are verbs. When you change ‘believing’ into its noun equivalent you
        get a clear object, a belief. When you change ‘knowing’ into its noun equivalent you do not get a clear object.
        Knowledge is an object word whose space is filled by more than one thing. This is an insecure basis on which to
        act. We explore this field below but our efforts are not helped by our current cultural bias. We have great
        respect for ‘knowing’ while at the same time avoiding ‘belief’ in talk and thought (outer and inner dialogues).
        <strong>Indecisiveness and self doubt are major problems of modern culture because of its emphasis on knowing
        rather than believing.</strong>
      </p>
      <p>
        <strong>Below are some thoughts around these initial points.</strong>
      </p>
      <p>
        <em>As a language game set in the contexts of its meaning cluster, belief is not about the reasoned truth of
        statements.</em>
      </p>
      <p>
        The main conclusion from our initial points is that belief is strongly attached to our being.
      </p>
      <p>
        I am looking out of my window in Sheffield to see if a sparrow hawk will fly past. I can do this intentionally
        without believing that I will see one. Indeed my inner dialogue will be busy telling me all the reasons why I
        will not see one. If someone asks me what I am doing I can say truthfully that I am looking for a sparrow hawk.
        I am looking without believing that I will see one. If I look believing that I will see one my inner dialogue
        will then be full of all the reasons why I might see one. I will also spend more of my time watching out of my
        window.
      </p>
      <p>
        Belief is far easier to understand than you think. It works very much like animal action does. When reading the
        <a href="movement.html">movement</a> and action topic we are likely to find ourselves overwhelmed by the
        complexity of the integrated movement models that lie behind the physical action that we observe others doing -
        and that we also observe ourselves doing thanks to our minds. Through live meaning clusters (<a href=
        "meaning_and_grammar.html">meaning and grammar</a>) you can strengthen your muscles to some extent by thinking
        of moving them without actually doing so. Dreaming of walking in dream sleep can, in some people, flick the
        action switch. They then walk though asleep. Belief works in exactly the same way. If you believe something to
        be true many aspects of yourself will commit to it. Even processes in your body inaccessible to both your inner
        dialogues and your visual imagining can be affected. In the meaning and grammar topic we outline the difference
        between semantics and meaning clusters. Belief is moving from the semantics of a language form to its meaning
        clusters.
      </p>
      <p>
        The power of belief is best seen in the placebo and nocebo affects. This power is not subject to reason or
        knowledge. It is more likely to make use of them than bow before them. It is as free and wide ranging as any of
        the other strange collection of processes that make up our biology. Its workings are most clearly exposed when
        researching a new drug.
      </p>
      <p>
        Even in the mid twentieth century doctors would sometimes prescribe bland inert pills to patients because they
        could not do any harm. The patients believed in them and their condition improved.
      </p>
      <p>
        You are more likely to know about the placebo effect than the nocebo effect. In the placebo effect you believe
        that something is doing you good and in the nocebo effect you believe that it is not. They are equally
        powerful. It is a very serious matter for any living thing to be injured or sick. In nature any weakness is
        life threatening. Living as we do in the glorious nursing and entertainment home, we have culturally dedicated
        ourselves to staying alive at all costs for as long as it is possible to do so whatever our state or condition.
        We see this as part of our commitment to life and against our old adversary death. Medicine fights on many
        fronts to do this using a great deal of knowledge. When new drugs are trialed, the belief processes of people
        are so strong that they are often more powerful than the effects of the drugs. The effect of the drug can only
        be seen if some people are given dummies and others the drug. We can get some idea of the wide ranging and
        subtle nature of belief by the fact that even the doctors administering the drugs cannot know which is a dummy
        and which is a drug. If they do know, they unconsciously strengthen the placebo effect for the drug and weaken
        the effect for the dummy. This is a simplistic view of modern research but it serves to illustrate the power of
        belief.
      </p>
      <p>
        Two vital points arise here. In our current culture the power of belief is always being questioned when it
        comes to faith healing but embraced in research design and then hidden at the point that a drug is actually
        prescribed. The only honest way to give someone a medicine is to say ‘I am prescribing you this drug but it is
        very important that you believe that it is doing you good’. The second point is that doing or saying something
        where you withhold belief; e.g. looking for the sparrow hawk when not believing that you will see one, or not
        knowing whether the drug will help you or not; is very different than holding a counter belief such as
        believing that the drug will not help you or even that it will do you harm. The strong link between belief and
        action is illustrated here as in the latter instance you would not take the drug.
      </p>
      <p>
        When we believe we commit to live meaning cluster content deep in our in our brains. We do this in many
        different ways in many different situations when subject to many different forces and influences. When our
        belief is questioned by ourselves or others what we really do is start a ‘why’ language game: anyone who has
        encountered a young child who has discovered the way to use the word ‘why’ will recognise that this game has no
        way out. Questioning a belief rarely reveals much about the set of conditions; the state of our experience;
        that caused the switch to be flicked. Indeed we are often aware of the differences between the inner dialogue
        which accompanied our flicking of the belief switch and the later inner dialogues that justified it to
        ourselves as well as the contrast with the reasons we give others for our belief.
      </p>
      <p>
        When we act we cannot predict exactly what the consequences will be on the physical and social reality that we
        inhabit. When we act we create an encounter in the physical and social worlds. We only experience these worlds
        through encounter and we have no other means of connecting to them. Belief strengthens the preparation to act
        and turns it into a base strong enough for action.
      </p>
      <p>
        The core meaning behind the word truth is revealed when we understand the core meaning behind the word belief.
        The first step is to separate the word truth used of our own belief from its use to refer to a belief held by
        someone else. If you want to follow up this point then read example two in the <a href=
        "speech_and_language.html">speech and language topic</a>.
      </p>
      <p>
        Belief is a commitment to a thought. For the body holding the belief; making the commitment; the flicking of
        the switch creates truth. All beliefs are true to the person who holds them. I believe that all white mice
        squeak in the same way. Because I am committed to this belief I am also committed to the truth of the statement
        ‘all white mice squeak in the same way’. Why do I believe this? I can’t remember. I have not spent any time
        looking after white mice. I suppose that I once read it in a trusted source. Only I have a feeling for what
        this commitment involves. You are likely to think that this is a playful and low commitment belief that would
        leave me with little more than a feeling of embarrassment (and only then if I had ‘gone public’ on it) if I
        changed the belief to ‘I think that white mice have slightly different squeaks’. So; I got it wrong then? No; I
        changed my belief. I am happy with my new belief but if you like I admit that the statement that ‘all white
        mice have the same squeak was wrong’. Note that we change beliefs and that the technical truth or falsity of
        statements is secondary as it is more about grammatical structure and logic. By the way you were wrong about my
        low level commitment to the original belief. It cost me in many ways as I had invested time and money in
        setting up a company to sell precision squeaking white mice.
      </p>
      <p>
        Beliefs, those things that we do and do not believe, are generally passed on socially from people and sources
        that we trust. Less often they are forged from our own experience.
      </p>
      <p>
        Knowledge is passed on in the same way as belief and in many contexts we generally do not bother to distinguish
        beliefs from knowledge but for Wittgenstein the failure to do this is central to the problem of the cultural
        over extension of scientific thought.
      </p>
      <p>
        <strong>What about knowledge?</strong>
      </p>
      <p>
        Belief switches between could and could not. It is like action we either act or we don’t. The core meaning
        cluster of knowledge is the place of hypothesis; the place where we think and observe but do not know whether
        to believe something is true or not. The whole edifice of knowledge is underpinned by the possibility that what
        is currently thought to be true could just as easily be found to be false. This applies to history as much as
        it does to physics. My search for a sparrow hawk by looking out of my window is independent of whether I
        believe I will see one or not. Though, as we noted earlier, what we believe will affect how we act while
        testing a thought. Keeping thoughts hypothetical is very difficult as when living things act (and as a result
        generate an encounter) no part of them can be left behind as they are one body. They are totally committed.
        <strong>Belief is the thought (inner dialogue) equivalent of action and often paired with it at the point that
        we ‘do’ something.</strong>
      </p>
      <p>
        Knowledge practitioners are required to speak in language games which, though authoritative constantly put
        forward provisional, conditional and qualified positions. All knowledge is ultimately dependent on evidence
        bases. Being human, the lives and beliefs of knowledge practitioners become mixed up in their judgments and
        they find it hard to keep up an uncommitted and open perspective; though, at the same time, they would strive
        to resist the assessment of themselves as being believers.
      </p>
      <p>
        <strong>The conditional nature of knowledge comes from the fact that it relates to action in a different way
        than belief.</strong> We have already noted that thinking about walking prepares your muscles for walking and
        changes the body’s chemistry leading to an improvement in muscle tone. It is in <strong>preparing</strong> to
        act that knowledge has its original and core meaning cluster. Who knows how to unlock this door? Who knows when
        the young swallows leave the barn? Who knows why there is a police car at the bottom of the street? <em>The
        last two are more distant from specific actions but they could inform a wide range of individual or connected
        actions.</em>
      </p>
      <p>
        From a knowledge perspective the truth and falsity of statements is a way of recording and describing a
        changing picture. The hypothetical disengaged position is vital if knowledge is to pursue its search for
        evidence and relevance. The application of knowledge is a very different thing from belief whether building a
        bridge or setting up a scheme for the unemployed. Actions are inherently unpredictable whether they are based
        on knowledge; ‘we must do something’; or belief; ‘we must do this’.
      </p>
      <p>
        Knowledge and belief are questioned in the same way. When we ask the questions ‘how do you know that’ and ‘why
        do you believe that’, the language games used hinge on replies that name trusted sources, share mental pictures
        and talk of personal experiences or present evidence?
      </p>
      <p>
        As we have seen the nature of belief is that to believe something is to ‘commit’ to it in the same way that we
        do when we act. This is not surprising as we have the option of acting on a belief. A commitment to a thought;
        for example ‘eating potatoes with lemon juice will give you energy without putting on weight’, creates a belief
        and this changes the relationship between the words used to express the belief and the original meaning
        clusters behind those words. When we flick the switch, the meaning clusters are trimmed and hardened up to be
        strong enough to support and defend our actions. In this case the act of eating potatoes with lemon juice. Our
        brain also sends out messages throughout our body in relation to the practice of a belief and the required
        involvement of the body; as seen in the placebo effect. This is achieved through the meaning clusters that lie
        behind every sentence, phrase and word that verbally expresses the belief. They are our experience register and
        contain the links to feelings, emotions, senses, episodes and beyond into the systems hidden from
        consciousness. Though we can express similar meanings using different language, the hardening up of the meaning
        clusters that takes place when we believe also has the effect of hardening the expression of those beliefs.
        This is the point at which we become tied to expressing belief using specific language. The danger is that the
        human culture will then recognise the belief only by a form of words. At this point the meaning clusters have
        shut themselves off from semantics.
      </p>
      <p>
        Belief functions also in a social animal context as the action that we are talking about includes both physical
        action and social action. Can you convince someone else to believe the thought? Do you have sufficient contact
        with the person or persons that you received the believed thought from? How can you build, or where can you
        find, the related thoughts that can also be believed and extend the reach of the belief further across human
        affairs? &nbsp;
      </p>
      <p>
        Beliefs operate at all levels of our inner dialogues and hence across all our actions. They engage with the
        particular and the general. If we are unsure of their social context, or know them to be unfashionable, we will
        generally offer our beliefs up tentatively and, in tune with the current cultural triumph of the hypothetical,
        we often say ‘I think’ to mean ‘I believe’.
      </p>
      <p>
        Our beliefs change as the shape of our physical and social lives change. Little change is brought about by
        thinking.
      </p>
      <p>
        Being a social animal practicing a social process we can be fearful of being outside the consensus. We trust
        that the belief is true and can be justified to others. In reality this makes us more vulnerable to the
        exercise of power; subdued by the authority of the person and their ability to persuade and/or the popularity
        of the belief within our reference group. On the other hand we are capable of defending a belief in the teeth
        of overwhelming evidence and brilliant reasoning and some of us will embrace social isolation and even death
        rather than change a belief. These are beliefs that we are heavily committed to.
      </p>
      <p>
        The degree of our commitment to the belief and the degree of social support that we have for it, especially
        from those we share our life with, more usually determines the energy that we put into defending a belief even
        when our thinking throws up doubts. The degree of our commitment to a belief, and the nature of the other
        beliefs that it is structurally involved with, provides a measure of how difficult it is to change it. It often
        has to be undermined by changing to a social reference group that does not hold the belief and then by
        attrition; changing the memories and records of sense, emotion and inner dialogues and creating new ones with
        implicit counter beliefs. The final attachment to the belief can wither away but this can be more cleanly and
        speedily removed by ritual action. We explain the nature of ritual in the <a href=
        "introduction_to_topic_language_and_shared_attention.html">introduction</a>. New beliefs can also be quickly
        secured this way. Ritual is widely practiced in cultures and it is one of the major casualties of the current
        scientific material culture with its conditional, tentative and distant mentality. It is ignorant and
        suspicious of engaged human nature and experience.
      </p>
      <p>
        We live in a culture of knowledge and in a material world created by it. This means that the hypothetical
        dominates. This is a world of knowledge islands; systems and attendant experts. From what we have examined we
        can conclude that this will weaken our confidence in living and enfeeble us. To what extent can we live out our
        nature in hypothetical states not knowing what to believe and what to commit to, withholding action as much as
        possible? Belief and the strength and confidence for acting are under pressure. Beliefs that we would once have
        acted on and changed through experience are held as thoughts and never tested alongside action.
      </p>
      <p>
        Our experiences are loaded with numerous elements all participating at varying levels of intensity. Thus
        unpredictable, unfathomable and chaotic change underpins our existence and is constantly present. To be alive
        is to be disturbed to a greater or lesser degree. Belief and action are how we throw our bodies into this.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="gender_and_temperament.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next"></div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
