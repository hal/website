<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Emotional display
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="emotional_display">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Emotional display
      </h1>
    </div>
    <div id="content">
      <p>
        Understanding emotions is made more difficult in that we do not just feel emotions but we also display them.
        Surely people vary in this between cultures? They do, but this point just switches the balance between
        observing and suspecting the presence of an emotion. Cultures where expression of emotion is repressed do not
        change the animal ability to express emotion. Members of these cultures demonstrate the same range of variation
        of this natural ability as all humans when secretly observed watching films or when being tested on images of
        people expressing emotions. We all naturally express emotions in a similar (variablish) way. The face has
        numerous muscles, especially around the eyes. We have a natural ability and a capacity to learn and become
        expert at the way that different individuals express emotion, especially those who we know very well. We are
        constantly searching faces; backed up by ‘reading’ voice, posture and movement; for evidence of emotion. We
        need this evidence to help identify the disposition, and the nature of the actions, of the human we have
        encountered.
      </p>
      <p>
        We also display/express emotions for social and cultural reasons. When we encounter humans that are only at the
        acknowledgment stage of our relationship with them, one of the options is to smile and move on without saying
        anything. When someone is telling us about the death of their friend we may well adopt a slightly sad
        expression. Acting like a football supporter when sitting in the ground, is hard to resist. We can act on stage
        or in films and we can ‘put on an act’ in social situations. Children as young as three make an effort to mask
        disappointment when receiving an undesirable present. Actors often have to use their own <a href=
        "episodic_memory.html">episodic memories</a> in order to find ‘live’ emotional responses that will make their
        display more convincing.
      </p>
      <p>
        This whole area is extensive and complex in its own right. What we need to take from it is the fact that we
        can, and need to, express emotions when we are not feeling them and that we can, and need to, fail to display
        feelings that we are registering - experiencing. It is not easy to do this but mastery of emotional display is
        essential to the social world. Indeed experience can change how we disguise and display emotions throughout our
        life. We put effort into disguising; thereby managing; our emotional display <em class="underline">while at the
        same time</em> trying to read the emotional state of other people. As we will see later the extent to which we
        are dominated by the emotion of ‘social’ fear is a measure of the effort we put into managing our emotional
        expression while identifying the emotion of others. We can be caught in the pincer of these two processes. All
        of this without the need for a mind!
      </p>
      <p>
        The species solution to the intensity and complexity of emotional engagement is the blank expression. We learn
        to maintain a blank expression for two reasons.
      </p>
      <p>
        The first reason is so that together with not looking at people’s faces, we can disappear from the social world
        when we want to avoid an encounter. We are of course still present in the physical world. A crowd is the best
        place to see the two worlds that human beings occupy. Combining a ‘crowd’ with the social requirement to relate
        to each other creates considerable stresses as witnessed in gatherings and parties.
      </p>
      <p>
        The second reason is so that, with little effort we can control emotional display. It is interesting to observe
        how we emerge from this ‘blanked out’ state. Imagine being part of a group of strangers in a queue, on a bus,
        in a train or relaxing in a park. A shared experience appears, such as an announcement of further delay, the
        bus hitting a car, half an hour not moving or horse play on the swings, then begins a process of catching each
        others eye and exchanging glances as we start to acknowledge each others presence and enter the first stage of
        beginning a relationship.
      </p>
      <p>
        There is a problem. We recognise individuals by a specific brain modeling of what we see as we look at their
        face. This process is called ‘face recognition’ and we know where it is located in the brain. <em class=
        "underline">The need to identify people therefore draws us to look at faces and raises the possibility of an
        encounter.</em> <em>We need to remind ourselves that early information about possible identity comes from
        posture, movement and voice. Face recognition is literally face re-cognition. The same face is seen again.
        Episodes with this sighting in, become live as do the related episodic models. As most of us know, finding the
        phonemic pattern that we use to symbolise (name) the human behind the face takes longer and can be very
        difficult. We looked at word-finding in the <a href="meaning_and_grammar.html">meaning and grammar</a> topic.
        In the case of names it is a common problem because of the phonemic range used (especially the presence of
        features from other languages), the total number of possible names and the arbitrary nature of their context.
        The phoneme computer is taxed.</em>
      </p>
      <p>
        Where can we find humans who are stuck with expressing what they feel? Anyone who has spent time among toddlers
        will know that they have an obvious ability to express emotion and we instinctively know what emotion they are
        expressing. The ability of babies to feel the emotion of interest/excitement has been used to study how they
        develop visual imagery. As with all words, babies, infants and children learn emotion words by sticking bits of
        experience to them. They are helped by how well they grasp the meaning clusters for the words grammatically
        structured with the emotion (feeling) word and how well they build the meaning cluster for the ‘sentence’ they
        have heard. It is very difficult to put together a core meaning cluster for emotion words. A complex selection
        has to be pulled together from particular physical and social circumstances, habits of speech, the expression
        of the emotion in faces, posture, bodily movement and voice and finally the core of biological records
        (<a href="episodic_memory.html">episodic</a> and <a href="meaning_and_grammar.html">meaning</a> cluster
        memories). This last is the hardest part. The major difficulty being, that their parents and relatives follow
        personally adjusted social rules when displaying emotion and they often display an emotion that they are not
        feeling. It is easier to attach the word to the display than it is to attach it to the feeling itself. Indeed,
        as a social animal it is biologically more important to do this.
      </p>
      <p>
        When you know a child or intimate companion well, you can often feel that the complexity of their display
        reveals conflict between two or more emotions. Sometimes you can witness a shared encounter creating rapid
        changes in the face, posture and movement of an individual as first one then another emotion occupies the focus
        of attention and also wins out in the bio-chemical messenger struggle.
      </p>
      <p>
        Clear and strong emotions are generally needed to unambiguously demonstrate this. On a walk in the park with a
        friend and their dog, the dog is lost. It is found by the gate but is limping. Happiness, sadness and anxiety
        are likely to be in conflict. What you will also notice is that the currently dominate displayed emotion will
        coincide with the content of the dialogue. “There is Spot” (happiness). “Oh dear Spot has hurt his leg”
        (sadness). “I hope it’s not serious” (apprehension/fear). In this context the briefest emotion is likely to be
        happiness, as the benefit of finding Spot is overcome by the perception of injury. Sadness and
        apprehension/fear will stay around longer, the first to maintain the focus on Spots injury and the second to
        energise action to do something about it.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="emotions.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="feeling_and_speech_and_language.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
