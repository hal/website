<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Feeling and speech and language
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="feeling_and_speech_and_language">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Feeling and speech and language
      </h1>
    </div>
    <div id="content">
      <p>
        <a href="emotional_display.html">Emotional display</a> creates a split in the body/brain between the feeling of
        the emotion and its physical expression. This increases the challenge of using speech and language to refer to
        feeling and emotion within a shared experience.
      </p>
      <p>
        The question ‘how do you feel’, when asked outside of sharing an experience, reveals very little. Here are some
        common answers to the question ‘how do you feel’: <em>alright, fine, okay, fed up, low, stressed</em> and
        <em>upset</em>.
      </p>
      <p>
        The first thing that we would expect to find is the dialogue equivalent of the blank face (see emotional
        display topic). We find this in <em>alright, fine</em> and <em>okay</em>. Generally it would only be in the
        context of a relationship that we would reveal that we are low in energy. This is the next most common group:
        <em>fed up, low</em> and <em>stressed</em>. We have a biochemical messenger for our general energy level and
        the word mood has been borrowed and re-constituted to describe it. When in a low mood we might say or think
        ‘low’. When feeling that our encounters and inner conflicts are oppressive we might say that we are <em>‘fed
        up’</em> or <em>‘stressed’</em>. The only one of these words that appears to refer to emotion is
        <em>‘upset’</em>. It is actually used to refer to a mixed personal disturbance that includes emotions.
        Generally there is an identified cause. <em>Upset</em>, accompanied by an appropriate natural display, is the
        first word that children use to refer, at least in part, to their own emotional state. This is thanks to the
        fact that a meaning cluster can be built from the content of the experienced world i.e. there are identifiable
        causes of <em>‘upset’</em>.
      </p>
      <p>
        In the social uses of emotion words i.e. you learn to associate the word ‘sad’ with pets dying, the word
        ‘happy’ with getting a present and the word ‘interest’ with paying attention to something. <em class=
        "underline">Children master these associations easily, often well before they have made a link between the word
        and the display and the word and the feeling in their body/brain.</em> Adults use more complex and nuanced
        patterns to talk about feelings/emotions without revealing whether or not they are actually feeling them.
        Children have not yet mastered these. <em>Heard immediately after a parent comes back from the vet having had
        the cat put down, “of course I’m sad, can we get a rabbit now”?</em>
      </p>
      <p>
        A major barrier to understanding feeling and emotion is the language game that we play with the word ‘feeling’.
        Its meaning cluster is complex with a number of cores. In contemporary usage the whole structure is held
        together by a vast blanket that the mind throws over anything that it is prepared to admit that it cannot
        control or does not want to take responsibility for. <em>This is the inner dialogue (thinking) version of the
        blanket.</em> The dialogue version of the blanket is thrown over anything that the individual human does not
        want to account for to others.
      </p>
      <p>
        As ever, language is the same whether used inwardly or outwardly. <em>I just felt like it. My feelings got the
        better of me. He/she/ it made me feel ‘x’.</em> An impressive example of the ability of the use of the word
        ‘feeling’ to knock back the enquirer, or the mind, can be seen when it is set against the infinite reach of the
        ‘why’ question. <em>“I have had enough of boot camp”. <strong>“Why”?</strong> “I have a feeling about it” –
        meaning that it is not available to language and/or I do not want to talk about it. <strong>“Perhaps it was the
        wrong time of day”.</strong> “Let’s talk about something else” – or the person just changes the topic.</em>
      </p>
      <p>
        <em class="underline">In order for the word ‘feeling’ to continue to act as a defence against shared attention;
        and directly stand as a thought stopper; we cannot expect any change in how we use it. The practice that we
        have adopted here is to use the word feeling to refer to states of the body/brain that can make it into focused
        attention (awareness), however briefly.</em>
      </p>
      <p>
        Firstly a contextually appropriate and brief reminder about what we mean by focused <a href=
        "attention_consciousness_and_self.html">attention</a>. Focused attention is experienced as a continuous state
        of wakeful awareness with varying content provided by a wide range of biological registers. Outside of the
        awareness there are flexible models controlling access but the strength of the individual register, in both
        relative and absolute terms, drives its appearance in focussed attention. All complex animals have this
        awareness in a way appropriate to their nature. Only we can be aware of the awareness because only we can refer
        to it in inner dialogue – we can name it - i.e. looking at a dog can be followed by thinking ‘I am looking at a
        dog’.
      </p>
      <p>
        On both counts we can be wrong about seeing a dog. The possibilities of ‘wrongly’, in the case of seeing a dog,
        range from an inability to understand a particular artist’s intention as we look at a picture of a dog, to
        calling a dog a cat as an infant still learning how to use the word ‘dog’, not seeing clearly enough the animal
        that just moved down the alley and disappeared into the scrub and not recognising a particular breed as being a
        dog. If we can be wrong about an animated external object so close to human experience then the scope for error
        when naming an experienced feeling, is considerable. Don’t forget that we learn what internal biological
        register to stick a word to by how other people use the word. Are our <a href=
        "meaning_and_grammar.html">meaning</a> clusters like those of other people? We know that young children can use
        the word ‘surprise’ but we also know that, on average, they do not ‘really’ know its meaning until they are
        around ten years old. Initially a surprise is a treat. This is sometimes exposed by language such as ‘can I
        have a surprise after school’. The children involved would not assume that this could include a surprise visit
        to the dentists.
      </p>
      <p>
        We might have connected the right biological registers to a word but it is not possible to accurately refer to
        our own feelings unless they make some appearance in focussed attention. We can say to ourselves that we are
        tired when in fact we are hungry, overwhelmed when we are embarrassed, lethargic when we are anxious and
        ‘alright’ when we are annoyed. <em class="underline">What is felt is as much about personal, social and
        cultural dialogues as it is about what we actually feel.</em> Nevertheless, used factually or fictitiously the
        meaning of the words themselves point to particular content that <strong>can</strong> appear in our awareness.
        Content that <strong>can</strong> be directly experienced. When the register of what we call ‘being tired’
        appears in our focussed attention there may or may not be recognition of this by our inner dialogue. In other
        words we may or may not think ‘tired’. Feelings do not necessarily trigger thoughts even when they come close
        to being verbally expressed. <em>‘You seem sad’. <strong>‘I suppose I am.’</strong></em>
      </p>
      <p>
        In addition, feelings spend most of their energy informing the body/brain beneath the level of awareness
        (focussed attention). Their constant presence at different degrees and their struggle with each other to
        influence actions, defines the lived moments of every animal.
      </p>
      <p>
        A casual search of any thesaurus reveals that many words seem to have been devised as a means of commenting on
        others rather than for the purpose of referring to our own condition. This is not surprising as <em class=
        "underline">we are arguing that inner dialogue – thought/mind - is designed as the internalised receiver and
        producer of material for social dialogues and not as a means of building a stronger and more ‘fit’ individual
        animal.</em> Socially isolated infant humans cannot acquire ‘proper’ language in the first place and barely at
        all if they have missed the key biological opportunity; naught to four. In other topics we show that in order
        to be able to develop language the human infant comes equipped with some remarkable biological features. We are
        not talking of a blank slate written on by other humans but living in intimate relationship with them as
        biology unfolds.
      </p>
      <p>
        I feel: tired, achy, stiff, lost, puzzled, confused, energetic, lethargic, dreamy, constipated, sick, randy,
        thirsty, hungry, overwhelmed, pleased to see you, glad you have returned, affectionate, tender, proud, jealous,
        embarrassed. This selection of words is designed to avoid words for feelings that you would immediately
        recognise as part of the six biological emotions that we examine in the emotion topic.
      </p>
      <p>
        We will pick out a word with a very strong feeling/biological register which readily shoves its way into
        focussed attention. The first thing that we have to establish is a separation between referring to feelings and
        the physical nature of feelings. This can be most easily achieved by examining ‘feeling sick’. It is hard for
        anything other than the feeling of ‘feeling sick’ to grab your focus of attention when your body/brain is about
        to eject the contents of your stomach. Indeed inner dialogue is pretty reliable at also coming up with the word
        ‘sick’. This does not mean that in dialogue you say that you feel sick. You will be speaking in the social
        world and this shapes what you say.
      </p>
      <p>
        Feeling sick seems simple to the mind, but the physical world bio-chemical registers, and brain modeling,
        needed to switch on this feeling, and also the physical action of being sick, are extensive and subtle. Being
        sick is widely available in the animal kingdom and there is evidence in many species of ‘taking’ emetics
        (usually by eating certain leaves) which implies that they also feel sick. The feeling itself has a range of
        registers and we have a limited ability to refer to this. Feeling sick is ‘stronger’ than feeling queasy. The
        feeling is created by the tightening of the muscles of the stomach without at the same time tightening those of
        the abdomen generally. <em>This wider tightening is a characteristic of fear which can; though generally
        doesn’t; make you feel sick.</em> When you feel queasy the muscles are less tight than when you feel sick.
      </p>
      <p>
        Inner dialogues can search inner verbal accounts of verbal, semantic (meaning cluster) and episodic memories
        for the presence of other registers that often appear in episodes with ‘sick’; for example fever/high
        temperature, the act of eating and the presence of disgust.
      </p>
      <p>
        This search will also be separately carried out by your brain and anything strong enough to make it, however
        briefly, into focussed attention may be picked up by inner dialogue. It will usually initially appear as a word
        or image. For example a smell register is more likely to partner an image of an object with that smell, or the
        word for that object, before it stands a chance of getting into focussed attention.
      </p>
      <p>
        The mind; inner and outer dialogues; can make us feel sick; by tightening the muscles in the stomach; when
        there is no body/brain register otherwise supporting the feeling. I am feeling queasy as I write this section.
        If I had used the word sick briefly I would have employed the simple verbal and semantic models for it but the
        more I think about it, and the more it monopolises focussed attention, the more I arouse my brain based
        episodic re-cognitions of both feeling sick and being sick. These are dominated by tightening the muscles of
        the stomach. <em>You need to remember here that animal episodic memory is re-cognition so real things have to
        be energised.</em>
      </p>
      <p>
        There is also a route where even the brief verbal/semantic model can lead to me feeling and being sick without
        any reference to my episodic experiences. ‘The mushroom that you have just eaten is poisonous’. In itself, this
        is unlikely to make me feel sick but it is highly likely that I would take an emetic. In summary, the animal
        process (this includes us) begins with bio-chemical registers that lead to tightening of the stomach muscles.
        <em>This is what we name as feeling sick.</em> The brain/body can then trigger the action of being sick or the
        action of finding and eating an emetic followed by being sick.
      </p>
      <p>
        The word sick is also used as a substitute for the word ill. In this use it does not carry the meaning of
        regurgitation. The meaning cluster of feeling sick already has a model form and it can therefore carry meaning
        into new symbolic models where the word is then used analogically and metaphorically. The grammar builds the
        model and the meaning clusters muddle together. I was sick that we lost the match. It is sickening how they are
        being treated.
      </p>
      <p>
        How does just using language, free of relevant context interact with bodily processes?
      </p>
      <p>
        Perhaps the most interesting phenomena, is the way that referring to a feeling can lead us to physically
        feeling it. My feeling queasy was triggered the more that I thought about feeling sick. This is because the
        biological/physical registers in semantic memory (meaning clusters) are ‘live’ and once energised they then
        activate their partners in episodic memories. This can include <a href="movement.html">movement</a> muscle
        features (<em>explained earlier</em>). You alone in the animal kingdom can make yourself tired just by thinking
        ‘tired’. Don’t stay too long on a word/phrase/sentence or it might ‘get you’. All other animals can only
        trigger biological registers from direct experience interpreted by the complexity of their episodic and
        movement registers/models/memories. This, allied to muscle tension, is the mechanism behind psychosomatic
        disorders but it is something that we all practise without realising that we do. In the movement topic we note
        that muscle tone can be improved by thinking of moving the muscle/part of the body. This is exactly the same
        process at work, only in the case of muscle tone the co-responders are movement memories rather than episodic
        ones. You can go beyond the word ‘tired’ to believing a grammatical model, and the meaning cluster it pulls
        together, ‘I am tired’. The topic of belief; belief defined as commitment to a thought; shows that placebo and
        nocebo are explained in the same way. There is a part of the brain that has been identified as being active in
        belief/placebo effects. It probably sends some kind of biological messenger around the body/brain.
      </p>
      <p>
        In unraveling the relationship between language and bodily processes further, we start with the wide field of
        general social interaction. Here we may stay at the level of verbal patterns and poorly grounded references as
        they pursue their main tasks of social animal cohesion and shared material efforts. Word meanings will be
        touched on at the point of simple generalised models in semantic memory (a light touch on meaning clusters). As
        an individual human participant it is very hard to remain at this level. It is at its easiest when using
        unusual words to refer to technical areas of the physical world but even here contextual words have physical,
        social and emotional experience stuck to them. We examine this further in the topic of <a href=
        "episodic_memory.html">episodic memory</a> using my own meaning cluster for the word phrase ‘Frisian cow’.
      </p>
      <p>
        <em>A quick reminder here of our understanding of semantics. Assuming appropriate attitudes, a social
        discussion about the meaning of a word will produce an agreed summary alongside similar use of the word by that
        social group. The word ‘similar’ is the key to grasping the reality. We only have our own meaning clusters
        which vary according to our experience. We cannot distinguish personal experience of words from something
        called their true meaning. That is why we use the phrase ‘meaning cluster’ as well as the word semantics. The
        word semantics refers to how meaning ‘looks’ within the shared mind. <em class="underline">The phrase ‘meaning
        cluster’ refers to how meaning ‘looks’ within the individual brain.</em></em>
      </p>
      <p>
        With all language all the time, there is the possibility of arousing co-responders in episodic memory for the
        participants, or for the individual if alone in an inner dialogue. Some of these will be held in language, in
        story form, but below this, real biological registers will be activated in the ‘story’ and ‘partnering’ aspects
        of episodic memory: real images, real tastes, real sounds and real feelings.
      </p>
      <p>
        Clearly there is a difference of degree between the amounts of strength in a biological register when it is
        re-cognised by semantics than when it is re-cognised by direct experience. I might feel a little queasy as I
        dwell on the meaning of feeling sick, but it is a small ripple. I might be able to improve my muscle tone a bit
        by thinking of moving a limb, but the effect is small. There is yet one more element. I may believe that
        thinking of moving my limb is strengthening my muscle. As a result I will probably do this more and the muscle
        will become stronger. In addition the belief in what I am doing will itself lead to a biological response that
        will further improve the muscle: the belief/placebo effect. I may believe that thinking of moving the muscle
        will make no difference but accept the social pressure to try it. I am likely to do the task less, but in
        addition, from what we know about the placebo/nocebo affects, my attitude will directly affect muscle tone
        improvement for the worse.
      </p>
      <p>
        We have started with the word sick because it relates to a strong biological structure where we can travel more
        easily between bio-chemical registers, action, focussed attention and language. We now have a model that we can
        use for all feelings. We are not going to examine the other words in the list in detail but some reflection on
        the group of them will lay out the territory.
      </p>
      <p>
        Even a quick look at the list we made points to the animal experience registers that lurk behind them. It would
        be possible for an extremely well equipped physician to find evidence of biological registers that corresponded
        to your reported feelings for the first thirteen words on the list. An account of how we use the words would
        also point to biological registers. Though it seems obvious, we need to remind ourselves that not being able to
        refer to a feeling does not mean that we do not feel it. A small mammal can feel biological registers akin to
        ours for tired, achy, stiff, thirsty, hungry, constipated and randy. I think that when we really understand
        biology we will find that it also feels energetic, lethargic, lost, puzzled, confused, dreamy, overwhelmed,
        pleased to see you, glad you have returned, affectionate and tender. <em>There is a true story about crayfish
        in the <a href="emotions.html">emotion</a> topic that might convince you.</em> Our little friend is not social
        animal enough to feel proud, jealous or embarrassed.
      </p>
      <p>
        In attributing feelings to animals we run into a strange bias in our thinking. We are happy to credit animals
        with a brain and even describe them as ‘thinking’ but are very reluctant to credit them with feelings and
        emotions. There is in fact no case for attributing thought to animals but, as we see in the <a href=
        "emotions.html">emotion</a> topic, it is a certainty that our small animal can feel sad.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="emotional_display.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="morality.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
