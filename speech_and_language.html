<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Speech and Language
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="speech_and_language">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Speech and Language
      </h1>
    </div>
    <div id="content">
      <p>
        We begin this topic with quotations from Wittgenstein followed by real experience examples.
      </p>
      <p>
        <em>Language sets everyone the same traps; it is an immense network of easily accessible wrong turnings</em>
      </p>
      <p>
        <em>Language can always make new and impossible demands – we are seduced into asking the same questions</em>
      </p>
      <p>
        <em>We are engaged in a struggle with language</em>
      </p>
      <p>
        <em>Nothing is as difficult as not deceiving oneself</em>
      </p>
      <p>
        <em>Language is a refinement, in the beginning was the deed</em>
      </p>
      <p>
        <em>The problem is the difference between understanding the subject and what most people want to see’…. ‘When
        something is well hidden it is hard to find’</em>
      </p>
      <p>
        Example 1
      </p>
      <p>
        A simple social game can reveal how illusory is the confident assumption that what we say carries a clear
        picture of the real world. Sit yourself and a partner either side of a table then make a temporary barrier
        between you so that you cannot see one another. You can use a propped up piece of cardboard, a tray, a large
        suitcase or anything to hand that does the job. You and your partner have a blank sheet of paper in front of
        you. You draw a quick child’s picture of a scene. This might include hills, roads, bridges, houses, trees,
        ponds and clouds. Keep it simple. When you have finished start telling your partner, through the screen
        barrier, how to draw exactly what you have already drawn on your paper. You will find that your language will
        be using a mixture of three core language topics. The first is descriptive naming and labeling. You will be
        using words such as bridge, house and chimney. You will clarify with reference to the number of windows and
        further description such as ‘a humpbacked bridge’ and ‘the road goes up the hill into the distance’. The second
        language topic involves you in talking about the space on the paper; ‘in the right hand corner about two
        centimeters in from the side’ and ‘above the X and slightly below the level of the Y’. The third language topic
        is not often used as it slows down the task. It brings in an attempt to direct movement. ‘Put your pencil
        halfway up the side of X and draw a line at a sixty degree angle up to a point six centimeters down from the
        top right hand corner of the paper’.
      </p>
      <p>
        You will be surprised that your partners’ drawing bears little resemblance to yours. You will blame them for
        failing to listen to what you said. They will blame you for not giving adequate instructions. The truth of the
        matter is that we assume that other people hear the same as what we say. We assume that there is a common
        meaning that can be built into a picture of the real world. Society gets on well with this assumption and mops
        up most of the mistakes and spillages efficiently but in truth language struggles to reveal anything
        substantial about reality. It is poorly connected to it. This has grave consequences for our inner dialogues.
        Is the natural state of the human mind delusional? True we have sensation and emotion but the only thing that
        connects an animal to reality is physical action and reality can only be revealed through the consequences of
        physical action.
      </p>
      <p>
        Example 2
      </p>
      <p>
        Let’s play for a moment with ‘the sun is shining on me’. Our <a href="meaning_and_grammar.html">grammar</a>
        means that we think, speak and write all the time as if we are relating to an object or state. This is true
        even if we are sharing feelings and experiences or engaged in idle chatter. At the same time we hold the belief
        that our words are our own. They occurred to us and they are from us. This is partly an illusion as largely
        what is happening in silent verbal thought is that we are simply ‘talking to ourselves’, albeit using speedy
        shortened verbal routes. We learn language as a means of social exchange. Verbal thought is a vital but
        secondary development.
      </p>
      <p>
        In the account that we are going to examine, ‘the sun is shining on me’ comes from a situation where, with
        others we are sitting together in a room with windows or in a glade which is a mixture of full sun and shade.
        One of a number of people present has the thought ‘the sun is shining on me’. They are reflecting on the
        personal experience of the sun’s rays falling on them. Let us imagine that they enjoy the light and warmth of
        the sun. They look around and notice that others are in direct sunlight. They then think ‘the sun is shining on
        them’. There is a world of difference between these two statements. This second thought is thin and sparse. The
        content of experience that it carries is minute compared to the initial thought.
      </p>
      <p>
        Both statements are true. They are statements about the real world. Nothing more can be concluded about the
        second statement. All else is speculation. It might look as if some are enjoying the shared experience of ‘them
        in direct sun’ but they may be about to move position into the shade feeling too hot or they may have fallen
        asleep. The content of experience behind the first thought, ‘the sun is shining on me’, is huge by comparison.
        It involves feelings and sensations in the body and on the skin. It involves the appreciation of visible light
        distinguished from shaded areas and often the strange sight of particles of dust suspended in the lit air.
      </p>
      <p>
        The difference between the two statements is not given by whether they are true or not. They are both thought
        by the same person during the same brief event. The only thing that appears to change is that ‘me’ is replaced
        by ‘them’. Using language to reflect on and talk about ‘me’, or personal experience, is a very different use of
        language than talking about ‘them’ in that the weight of meaning changes. We are still using the borrowed tools
        of common language but in the experience of ‘the sun is shining on me’ is a verbal thought that itself adds to
        the quality of the experience and itself links to new sets of feelings and memories. The verbal thought
        increases the weight of the experience. This is reality. Any links to the words ‘ the sun is shining on them’
        are speculative. They are imaginary. They are insecure. Even asking one of the others in direct sunlight if
        they too are enjoying the sun does not mean that you have left the imaginary to join the real as what they have
        said is in answer to your question. They may not have been aware of enjoying it until you asked. They may say
        they are out of politeness to you. The real is only what is experienced. Words describing experience are
        pregnant with meaning but those that do not are light and thin. You cannot tell the difference in daily talk
        and thought, though you may have your suspicions, as to use the same word is ‘taken’ by us all to mean the same
        thing. We learn talk as a means to share and then do other things with it.
      </p>
      <p>
        Example 3
      </p>
      <p>
        A personal experience helps to illustrate how hard it is to separate talking about something from knowing it
        and experiencing it. It comes from my job as an educational psychologist. I specialised in assessing children
        who were thought to have speech and language problems. The member of staff who knew the child best watched as
        we went through some tests. There was a pair of tests in particular that could reduce the staff to incredulity.
        Some children were able to illustrate that what the staff assumed that language did was wrong. Both tests are
        based on pictures. In the first one, groups of four pictures are shown to the child and they are asked to point
        to the one that is, for example, ‘car’. The four pictures are of a train, car, lorry and cycle. The four
        pictures are chosen very carefully and it is possible to assess abstract word meanings through thoughtfully
        selected and drawn pictures of actions, scenes and facial expressions. It is possible to ‘test’ the child’s
        understanding of very difficult words once they have ‘got the idea’ and the vocabulary of fifteen year olds can
        be tested with words such as ‘invertebrate’, ‘solution’, ‘perplexed’ and ‘barometer’.
      </p>
      <p>
        The second test also uses pictures. The child is shown two pictures, for example a car and a road, and told
        ‘this one goes with this one’ – using pointing gestures. The child is then shown a train and asked ‘which
        picture goes with this’ – gesturing generally at a page with four pictures drawn on it? The four pictures are
        of ‘rails’, ‘road’, ‘cart-track (furrowed)’ and another type of train. The reason for having one picture in the
        same category (train) is to ensure that the child can resist grouping (finding another example of a set) and
        remain focused on looking for a relationship. This test can also get very complex after a run on easy items.
        The ingenuity of the test designers and artists can be considerable. The participating children need to have
        good shared attention (social) life modeling in order to take part in the tests. &nbsp;
      </p>
      <p>
        The first test is a simple way of crudely assessing that when you hear a word you know what it means. The
        second test is about showing that you can understand the relationship between pictures by choosing a parallel
        pair of pictures. Broadly speaking it is analogy. ‘These two things in this picture are in a relationship with
        one another (they go together). Now see if you can find which one of these pictures goes with this picture’.
        Most children will have similar scores on both tests. This tells you nothing interesting other than how they
        compare to their peer group. The few interesting children divide into two groups. In the first group are those
        who have much higher scores on the first test (receptive vocabulary) than the second (relationship pictures).
        The staff may be surprised but are not incredulous. Their assumption seems to be that understanding what words
        mean is key to functioning and being able to learn and reproduce knowledge stored in words is what education is
        about. They sometimes see the child’s failure as being a result of poor test design and believe that the right
        task would reveal the child’s ability to think. The staff tend to explain away the results on the assumption
        that knowing the meaning of words somehow equates with all understanding.
      </p>
      <p>
        The staff who are incredulous are those with the second group of children. These children can do well at the
        relationship picture test but prove to have very poor vocabularies. The disparity can be huge. They have a
        language problem; a specific problem in acquiring an understanding of language. This does not however remove
        their ability to understand reality. The children who illustrate this best are between four and eight years
        old. The reason for this is that older children will encounter harder tasks. As the pictures get more complex,
        word searches (in reflective thought) are needed to expand the content of the pictures. The staff are generally
        aware that the child with the receptive language problem is somehow bright enough and seems to know what is
        going on. When they see their remarkable performance, they cannot believe that it is possible to have so little
        understanding of language and yet show an impressive ability to understand the world around you abstracted in
        pictures (how it works and what relates to what and in what way). The child’s understanding can only be
        demonstrated in practice or in a specially devised task. It cannot be shown by talking. But is ‘talking about
        something’ evidence of understanding?
      </p>
      <p>
        Let us return to the first group. The most extreme of this group, with good use of language and very poor
        understanding of the real world around them, can go on to have major behaviour problems and, in their teens,
        social and psychiatric problems. Rarely do the professionals involved escape the language trap. The parents can
        escape it. They know the child so well that they can believe the language/reality divide when it is illustrated
        by drawing on their own experiences. It turns out that as a culture, and maybe as a species, we cannot help
        assuming that being able to talk the talk always means you can walk the walk. We have a widespread and deeply
        embedded false understanding of ourselves and of the world of human affairs around us.
      </p>
      <p>
        Example 4
      </p>
      <p>
        Towards the end of my career I worked closely with a Child Psychiatrist. Over a period of eight years we
        identified sixteen children who baffled their schools and parents. We were very familiar with all the usual
        developmental difficulties and between us would cover the full range of assessments and insights. The children
        varied in age from eight to fourteen. They had good language skills and had no problem with grammar or
        vocabulary. You could have a sensible conversation with them on any age appropriate topic though limits
        appeared sooner than one expected. They seemed to understand descriptions of the physical and social world and
        could talk of how to act and what to do. As a result, nobody worried about their ‘intelligence’ but they did
        worry about their learning and their participation in home and school. There were some lessons and situations
        that they could concentrate on, or participate in, and others when they lost concentration and became very
        restless.
      </p>
      <p>
        The children’s behaviour was at its best when they were in routines that they were familiar with. They showed
        considerable delay in practical skills across all situations including in mathematics and writing. We began to
        understand that it was new practical activities and tasks that most exposed their difficulties. When they were
        not taking part their behaviour was challenging and inappropriate but this seemed to be directly linked to
        their disconnection from the activity they were meant to be doing.
      </p>
      <p>
        Instead of describing their limitations as being about social and emotional difficulties involving previous
        experiences, we wanted to dig deeper into their problems with new practical situations. We gave them a range of
        tasks for which we had data about the performance of their age group (within their three monthly age). We were
        very surprised that they did very poorly on one of the oldest ‘tests’ around, called various things but best
        described as block design. In this task, blocks with rectangular and triangular patterns on each face are used
        to copy a series of illustrated patterns. They all scored on the first or second centiles – the worst and
        second worst child in a hundred. On all other tasks they tended to score right across the average range. We
        made the link between practical skills and block design but could not see how this worked. We explain what is
        happening here in the integrated <a href="movement.html">movement</a> modeling topic.
      </p>
      <p>
        If we understand block design to be a test of integrated movement modeling then we can see that limitations
        here would affect all practical skills. One of the most powerful problems would arise when the teacher is
        explaining a new task or activity, or one that the children have little mastery of. The teacher is talking
        (‘pointing to the modeling’) and showing/demonstrating. Skilled teachers will be doing a lot of showing and not
        placing too much dependency on the challenge and vagaries of language. The children are identifying what to do
        and pinning down what things look like, but this is in order to build their own initial brain modeling for the
        task ahead. Our group of children would be really struggling. This group of children is rare because their
        grammar and language skills are at least average and this places them in a position to participate in speech
        and language while struggling severely in the physical world.
      </p>
      <p>
        Example 5
      </p>
      <p>
        Identical twins sometimes develop what is called twin language. It is rare for this to become a problem but
        twice it happened in my work. Included in my job was attachment to a speech and language unit in a primary
        school. We had referred, twin boys aged five from rural Sussex. They appeared to understand all that they
        personally needed from English but could only engage in free flowing conversations with each other. They used
        some poorly pronounced words and phrases when talking to parents and teachers and even forced their own
        invented vocabulary on others. They had spent their entire lives together. Their parents had not realised the
        importance of separate time for each boy to build intimacy and conversation with other people. They spoke to
        each other in a language unintelligible to others.
      </p>
      <p>
        When we had them in the unit and examined their language we discovered that its grammar differed from English
        and that the range of speech sounds also departed from English and included clicks. <em>Some African languages
        use clicks.</em> All children produce more speech sounds than required by their group language in the babbling
        stage. They had selected for their shared language clicks and different phonemes; <em>the latter probably in
        reality created by different phoneme boundaries and slightly non-standard tongue positions.</em> Like all of us
        they had core grammar but expressed it in their own personal grammar from a mixture of English and shared
        object-movement experiences and feelings. They had created their own language by setting their biological
        language registers, aided by their experience of English, into their shared life (shared attention life
        modeling).
      </p>
      <p>
        Example 6
      </p>
      <p>
        Someone who we will call A, is standing at a sink in a kitchen facing a window. A second person comes in, B,
        through a door behind them. The two people involved cannot see each others faces. Now identify with B. We are
        going to trace what happens in you between you hearing A say “did you get the eggs” and hearing yourself say
        “no, I have sinned”.
      </p>
      <p>
        <em>‘Did you get the eggs?’</em>
      </p>
      <p>
        I hear.
      </p>
      <p>
        Phones
      </p>
      <p>
        Even my hearing is set up to detect the physical profile of human speech and the physics is distorted to phones
        so that I can hear more easily what is said. Digital hearing aids amplify wave lengths but even a speech-sound
        friendly programming will amplify non speech sounds. Quite a significant loss of hearing is needed before it is
        worthwhile using a hearing aid as they impair phone recognition.
      </p>
      <p>
        Phonemes
      </p>
      <p>
        A millisecond behind the models searching for phones within the ‘noise’ are those searching for speech sounds
        (phonemes). A phoneme is a set of phones as in the ‘t’ sound. They also search for patterns of phonemes at
        numerous levels: syllables; words; phrases; clauses; sentences; known speech habits of the person talking;
        cliches and common and habitual ways of talking about things; to name but a few. The largest specific area of
        the human brain is dedicated to phonology (phoneme patterns). It is a horrendously complex process to hear
        language, when compared to hearing sounds.
      </p>
      <p>
        Grammar and meaning
      </p>
      <p>
        Milliseconds behind phonological detection models are a pushy gang who have already got forward brigades in the
        speech sound (phonological) detection team. These are searching for grammar and meaning.
      </p>
      <p>
        At all levels, I am essentially throwing these entire fields at what I am hearing and then desperately and
        simultaneously predicting; informed guessing; what the next phrase, word and sound will be. There are
        innumerable members of all these fields but I also have helpful social knowledge and experience. Having said
        that, it is common to not hear or to mishear what people are saying especially if you are expecting them to say
        something else or have to hold the sound sequence in verbal (phonological/inner dialogue) memory to search for
        the meaning cluster (talking in cliches is a lot easier).
      </p>
      <p>
        There is a crucial and vital feature here when seeking to understand human experience. The forward phoneme team
        searching for the sounds of words, phrases etc. is only interested in the speech sound patterns. Lets pretend
        that instead of answering A, I repeat what they have said “did you get the eggs”. This is a verbal process
        without an attempt to communicate meaning though the intonation may change in order to convey feelings. What we
        are referring to is the ability to repeat back what has been said without having understood it . Handy when
        learning a foreign language, reciting poetry, quoting a legal document, or recalling what somebody actually
        said to you during the course of an argument. It is the only place in our brain where there is anything that
        resembles a computer. Here, we use the phrase verbal memory to describe the ability to remember language
        without necessarily understanding what is meant.
      </p>
      <p>
        The exercise of the phonemic computer can also be seen when you read, either out loud or to yourself, without
        comprehending what it is that you are reading. We read to inner speech using the same process as inner
        dialogue. This is what makes reading and correctly spelling English so difficult. In English the relationship
        between the letters that are needed when we spell a word, or are recognised as a particular word sound when
        reading a word, do not have a one to one relationship with the spoken sounds of words. <em><em class=
        "underline">Tough</em>, I hear you say.</em>
      </p>
      <p>
        The teams looking for <a href="meaning_and_grammar.html">meaning and grammar</a> can bring vast programs to
        bear but their encounter capacity is limited by the timely need to make a response to the person standing in
        front of me. Theirs is a semantic process made up of the meaning clusters for words, phrases, sentences,
        cliches, expressive habits etc. and the grammar within which they are placed. People vary in all things and
        some are better verbally and give the impression of understanding while others use comprehension (moving
        through meaning clusters) to prop up their poorer verbal memory. They often remember what has been said but not
        the exact words.
      </p>
      <p>
        At each level; within each field; all the individual models and registers are looking for evidence of
        themselves - evidence that they exist. They are looking for re-cognition.
      </p>
      <p>
        We have got to the point where I understand ‘did you get the eggs?’ The social world now opens up and the mind
        really becomes engaged. Well, may be. It could be locked in an inner dialogue that it does not want to give up.
        I may be thinking about how to lose as little apple as possible as I cut out some bad bits before eating it.
        The mind may not have the focus of attention as I listen intently to music on the radio. The complex social
        world we all the time inhabit (even if it is only the one in our head) the current register of emotions and
        every other aspect of my condition will be potentially active at this point. My constantly disturbed existence
        is being disturbed by a new element.
      </p>
      <p>
        I now hear myself say ‘no, I have sinned’. How can I hear myself say it, as if I did not know that I was going
        to say it in the first place? I could have known that I was going to say it if I had thought it in inner
        dialogue prior to articulating it. We can blurt things out or we can hold/say them in inner dialogue and give
        ourselves time to decide whether to say them or not. Inner and outer dialogues are directly accessible to our
        meaning clusters which in turn have some access to our bodily states through what has stuck to the cluster –
        for example a smell memory originally derived from ripe brie and now attached to the meaning cluster for cheese
        or in this case a feeling of annoyance (very mild anger) when I am asked if I have done something. That
        nuisance the ‘why’ question also crops up. We can meaningfully ask ourselves ‘why did I say that?’ We can
        generate an answer to a why question for our benefit or for giving an account to others but in most cases the
        link between physical reality and the answer is unreliable. Let’s make assumptions. In this case, my feeling of
        annoyance about being asked ‘did you get the eggs’ and the fact of having forgotten to get them, formed a
        meaning cluster whose shape fitted nicely behind the words and grammar of ‘no, I have sinned’.
      </p>
      <p>
        We may or may not have known what we hear ourselves say but how did we manage to say it? Now have one of the
        most impressive things to achieve. We have to speak. The phonological pattern for the sounds needed is the same
        as hearing the words but this time they have to be the basis for the physical act of producing the sounds. This
        is an extraordinarily difficult physical thing to do. Our speaking apparatus involves rapidly changing tongue
        movements and mouth shapes, the closing and opening of the tube leading from the back of the mouth to the nose
        (when saying m and n), the vocal cord variations for the vowel sounds and the lungs acting like bellows. The
        whole production is a task for the fine co-ordination of our muscles. Speaking and singing are, at the point of
        production, entirely physical acts. The required phonological sequence of ‘no, I have sinned’ is converted into
        movement models governing my muscles. Children generally take a long time of development and practice to master
        the entire system: the distinction between the muscular production of ‘l’, ‘w’ and ‘r’ is mastered years after
        the vowel sounds. The Chinese do not learn how to do this at all, as in their language the phoneme is the same,
        but they do learn pitch perception and production because they have phoneme changes around pitch. Languages
        vary in their phoneme and muscular demands on the human body and brain.
      </p>
      <p>
        Interestingly, to describe a child who speaks well at a young age as intelligent is a misuse of the word
        intelligent. It would be more apt to say that they have excellent fine motor co-ordination and a good
        phonological ability. These can hide ignorance in children as what sounds like meaningful speech can be largely
        verbal memory with very thin meaning.
      </p>
      <p>
        Now read the <a href="meaning_and_grammar.html">meaning and grammar</a> topic.
      </p>
      <p>
        For the way in which speech and language is embodied within shared attention life models, read the <a href=
        "intimacy_movement_and_shared_attention.html">intimacy, movement and shared attention</a> topic.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="what_kind_of_animal_are_we.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="meaning_and_grammar.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
