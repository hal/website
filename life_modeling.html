<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Life Modeling
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="life_modeling">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Life Modeling
      </h1>
    </div>
    <div id="content">
      <p>
        [For more depth in this topic read read the topics <a href=
        "biological_registers_and_existential_principles.html">existential principles and biological registers</a> and
        <a href="variation_unpredictability_and_mathematics.html">variation, unpredictability and mathematics</a>.]
      </p>
      <p>
        <strong>Thoughts to take forward.</strong> <em>These points are examined in more detail at the end of this
        topic</em>
      </p>
      <p>
        Life modeling has no similarities with mathematics. In mathematics everything is exactly repeatable. In life,
        <em class="underline">nothing</em> is exactly repeatable.
      </p>
      <p>
        The words ‘modeling’ and ‘model’ are used because we cannot reflect on the nature of life without using the
        structure of language. We think and talk primarily in terms of objects and movements; nouns and verbs. This is
        not in itself original as they are both drawn from our animal vision where we find object and movement
        perception. Objects and movements are existentially deep forms and they are widespread within the brains of
        advanced animals.
      </p>
      <p>
        <em class="underline">We can only vaguely describe life models because they cannot be captured by mathematics,
        science or language.</em>
      </p>
      <p>
        ….……
      </p>
      <p>
        <strong>Life modeling expressions range from functioning within the complex biochemistry of smell through to
        animal to animal ‘face to face’ moments. Below are two illustrations.</strong> Firstly something that humans
        used to do with sheep - <em>not that one!!</em> Secondly the nature of a personal face to face meeting with a
        stoat.
      </p>
      <p>
        In the lambing pens of farmers (particularly hill farmers), some lambs would be dead at birth or die soon
        after. <em>The key is to get the lamb suckling from her mother.</em> The mother could be left with no lamb or
        only one lamb but she would still be producing milk. Sheep can suckle two lambs and sometimes three (if they
        are on good grass and the mother is a good milk maker). It is not uncommon for sheep to have three lambs but it
        is hard to suckle three successfully. Often one can grow up weak and not fatten up well. How about giving one
        of the triplets to the mother who has none or only one. This is what the farmers did - easy to say but
        extremely complex to do. In addition every time they did it they could not be sure that it would work.
      </p>
      <p>
        If you watch nursing sheep and lambs in a field, the lamb(s) and their mother recognise each others calls. Even
        so, when they are reunited the mother will always smell the lamb to check that it is hers. Newly born lambs do
        well to get on their mums nipples and are not yet in voice. The mother sheep can only use smell to identify its
        offspring. Simple then, the lamb has a specific smell which is detected by the mother sheep’s sense of smell.
        We think of a simple, ‘one to one’, physical process.
      </p>
      <p>
        What actually happens is that the skin of the dead lamb is put over the ‘third’ lamb and it is placed with the
        bereaved mother. Sometimes she accepts it as her lamb and sometimes she rejects it. After a few days the dead
        lamb’s skin is taken away. Sometimes the mother rejects the lamb at this point, on the grounds that it no
        longer smells like her lamb, but usually she continues to accept it. Either a lamb smells like her lamb or it
        doesn’t, so where lay the choice and how is it made?
      </p>
      <p>
        Smell is complex and contains lots of different elements - <em>a number of different pheromones of varying
        intensity would be part of the picture here</em>. The sheep does not recognise her lamb by detecting one simple
        element. She uses a brain profile; <em>the smell version of a life model</em>. This is more robust than a
        single element. It can cope with variations within the individual components as well as variations in the set
        of components. The lamb that is wearing the skin of her dead lamb does not smell the same as the dead lamb. The
        profile has changed quite a lot and she can detect some of the smell of the new lamb. The profile of the new
        lamb is <em class="underline">like</em> the profile of her dead lamb (thanks to the skin) but it is not the
        same. If her brain accepts that the likeness is within the range of variation tolerated by the profile (pattern
        or model) of her lamb’s smell, then it is her lamb and she acts as if it is. <em>It’s similar to recognising
        her lamb even though it has rolled in a cow pat.</em> Thanks to the elements and registers introduced by the
        ‘new’ lamb her brain profile of her lamb’s smell has also changed. The profile elements, that originate from
        the dead lamb, lesson as the skin dries out. They decline in significance but are still part of the profile.
        When the dead skin is removed from the new lamb the smell profile of her lamb changes radically yet again as
        the elements from the dead lamb are no longer re-cognised. The brain has now to accept that the new profile
        variation is within acceptable limits. If it decides that it is, then she has a totally new lamb with a totally
        new smell and is fine about it.
      </p>
      <p>
        The ‘time’ element of life modeling we can capture in an incident when I had a face to face encounter with a
        stoat who then looked away and moved of. Picture sitting at the foot of a large tree, with roots exposed,
        growing on a grass bank and set in a grass field. You turn, as you casually look behind, and find yourself
        staring into the eyes of a stoat with its rear end hidden behind a tree root. The distance was no more than
        eight feet. Time seemed to stand still but the mutual gaze lasted for about ten seconds. The stoat then turned
        away and moved off. I felt that I was looking at an equal, a sentient being. A potential acquaintance. For all
        animals with eyes there is a major difference between being seen by another animal and not being seen. At the
        very least it is crucial for hunting and for surviving being hunted. For all animals eye contact with another
        animal creates a space in time. It is accompanied by stillness. It can be milliseconds or minutes. What it
        shows is that ‘time’ is part of life modeling.
      </p>
      <p>
        <em>I am writing this account by drawing on the film (plus sounds, smells etc) of the incident in my <a href=
        "episodic_memory.html">episodic memory</a>. Note that you are able to picture the incident that I have
        described by using pieces from relevant sensory content in your episodic memory. If we then both painted
        pictures of what we had remembered or constructed they would be very different. Even if we had been both
        looking at the same stoat in the same place at the same time the episodic memories would be different. In life
        modeling everything morphs and slips away. Of course the stoat was also largely dependent on its episodic
        memories - its experience.</em>
      </p>
      <p>
        <em>The stoat and I met, face to face, as animals. If you look at the <a href="emotions.html">emotion</a> topic
        you will see that both of us were using the emotion of interest. Both of us went from the surprise end of
        interest down to a level we describe as interest. There was no evidence of other emotions such as
        apprehension/fear. The emotion topic also clarifies that there is an emotional landscape for every moment that
        we are alive.</em>
      </p>
      <p>
        What were we both doing as we looked at each other? When we pause in ways like this we often say that we are
        thinking but actually we are unlikely to be. We use the word thinking to refer both to inner dialogues and to
        other brain activity. In this website we only use ‘thinking’ to refer to inner dialogues. It is more likely
        that both of us were searching <a href="episodic_memory.html">episodic memory</a> stimulated by every register
        currently engaged. This would be outside of focussed attention as we are using this to to stare at each other.
        We were both doing versions of the same thing. I can remember ‘thinking’ after about seven seconds; as compact
        inner language entered my focussed attention weakening my engagement with the stoat - though I maintained eye
        contact. As no animal but us can think then the stoat could not have been thinking. Both our brains were using
        a time space to allow possibilities to arise both within the encounter and within the biological registers
        active in us. We were expressing animal life and using similar life modeling. It is stopping or slowing down
        existential time within parts of our body/brain, in the context of the continuing material time of the
        encounter, that makes it possible for all our ferociously complex biological registers to participate.
      </p>
      <p>
        How are pauses resolved? They are resolved when an action is initiated. In the case of the stoat and myself we
        could have simply looked away. The stoat looked away then walked away. The initiation of an action ends the
        ‘stand off’ of biological registers and life models. It directly leads to the release of dopamine which acts on
        the biological register for the emotion of satisfied/happy. We feel this. It is similar to the release of
        endorphin when we use our muscles. It does not just hide the natural tearing of muscles it also acts on the
        emotion of satisfied/happy. (<a href="movement.html">Movement</a>)
      </p>
      <p>
        It is so easy to see pauses in animals. The more complex the animal the more that it pauses. The larger the
        brain of an animal the larger its episodic memory and the longer the pauses it takes. Don’t confuse this with
        the fixed gaze of hunting actions as here focussed attention is fixed on expressing the action. Our need for
        pausing is so considerable; made extreme by inner dialogues; that for most of the time we appear to go around
        barely connected with the world around us. <em>This is partly explained by both social overcrowding and the
        movement friendly, and therefore more predictable, nature of our man made environment.</em> We appear to be
        fully animated only in engagements with each other and especially when taking part in social life models or
        when with high intimate individuals. Smiling and laughter take the energy out of errors and conflicted
        elements. To a lesser extent we can observe constant pausing very similar to ours in all primates. Primates are
        overwhelming social in nature. The social world is ferociously more complex than the physical world and highly
        dependent on empty material and existential time - pauses where there is nothing else demanding action.
      </p>
      <p>
        ….……
      </p>
      <p>
        <strong>How does this concept of pauses and empty time fit with the major life modeling areas that we
        summarised in the <a href="biological_registers_and_existential_principles.html">existential principles and
        biological registers</a> topic?</strong>
      </p>
      <p>
        To quote:
      </p>
      <p>
        <em>There appear to be four important ‘pinch points’ where conflicting biological registers and life models are
        resolved.</em>
      </p>
      <p>
        <em>The first is the movement trigger.</em> (A pinch point even though complex movement and action models can
        go on for some time). <em>We examine this in the <a href="movement.html">movement</a> and action topic.</em>
      </p>
      <p>
        <em>The second is focussed attention. We look at this in the <a href=
        "attention_consciousness_and_self.html">attention, consciousness and self</a> topic.</em>
      </p>
      <p>
        <em>The third is shared attention. Explanation of this can be found in the <a href=
        "introduction_to_topic_language_and_shared_attention.html">introduction</a> and in the <a href=
        "intimacy_movement_and_shared_attention.html">intimacy, movement and shared attention</a> topic.</em>
      </p>
      <p>
        <em>The fourth is ‘topic’ which you can read about in the introduction</em>
      </p>
      <p>
        <em>All of these use the same basic form of a <a href="life_modeling.html">life model</a>. These are highly
        flexible and in themselves as unpredictable as the encounters experienced by the embodied animal.</em>
      </p>
      <p>
        When looking at these topics consider the way that timing, pauses and the initiation of actions are at the
        heart of their nature.
      </p>
      <p>
        ….……
      </p>
      <p>
        Our personal lived experience also carries the sense of empty time - where time is just ‘space’. Do we all live
        with the deep feeling that we face an empty void unless we fill our waking lives with intimate, physical and
        mental activity of some kind? <em>This language might not work for you at all as we also ‘feel’ it according to
        our temperament.</em> Why have we got ‘intimate’ described as an activity? <em class="underline">Our greatest
        complexity, and therefore the most complex life models occur not within ourselves as individuals but within our
        participation in the social world that we are evolved for.</em> In the topics of <a href=
        "speech_and_language.html">speech and language</a> and <a href="memory.html">memory and grammar</a> we
        established that we have a variable biology for both of these but we also found that without experiencing early
        social participation they would <em class="underline">not develop</em> in any of us. In development, social
        dialogue precedes inner dialogue (thought). We attribute thought to babies and toddlers because ‘attributing’
        is part of our biological social task - we do it from our animal nature.
      </p>
      <p>
        ….……
      </p>
      <p>
        <strong>Here we re-visit the first section of this topic.</strong>
      </p>
      <p>
        We can only vaguely describe life models because they cannot be captured by mathematics, language or science.
      </p>
      <p>
        Life modeling has no similarities with mathematics. In mathematics everything is exactly repeatable. In life,
        <em class="underline">nothing</em> is exactly repeatable. We can also show that the origin of mathematics lies
        in our animal nature. Mathematics is not a priori; meaning that it does not exist without reference to reality.
      </p>
      <p>
        The words ‘modeling’ and ‘model’ are used because we cannot reflect on the nature of life without using the
        structure of language. There is a more fundamental problem at the heart of language. We think and talk
        primarily in terms of objects and movements; nouns and verbs. Both have their origins in animal vision where we
        find object and movement perception. Objects and movements are existentially deep forms and they are widespread
        within the brains of animals. Language is not a priori; meaning that it does not exist without reference to
        reality.
      </p>
      <p>
        In as much as science uses mathematics and language it is restrained by the forms and limits of both.
        Mathematics dominates science because of the accuracy and repeatability of its forms. It is at the heart of
        rational thought particularly when combined with aspects of language to form logic. We associate reasoning with
        logic but human beings are ‘surprisingly’ poor at logic. We are, however, very good at reasoning by comparison;
        reasoning by analogy. This fits <a href="episodic_memory.html">episodic memory</a> very well.
      </p>
      <p>
        In the range of topics that we are looking at we have many biological registers beyond vision and speech and
        language (<em>we need to remind ourselves that thought is only inner dialogue</em>). Our nature includes many
        other biological registers intimacy (nurture and sex), other senses, physical movement, episodic memory and the
        emotions. We encounter most of them on this website. &nbsp;
      </p>
      <p>
        Life is innately incomprehensible and that means not just in reflection but in every moment of being alive. We
        do not and cannot know what is happening. We cannot know why other people act as they do and we cannot know why
        we act as we do. We try to understand, comprehend and explain because they are essential for our intimate
        social lives. <em class="underline">Our dominate rational culture says that we can know.</em> We put the
        ability to know above hugging, smiling, laughing, apologising, frowning and a thousand ways of living.
      </p>
      <p>
        Life can only be lived and we need to bring back most of the life we have put to one side. Perhaps the only way
        to relate to life itself is to feel overwhelmed by it, at the same time as feeling the strength and mystery of
        life in oneself and in relation to others. Nothing does this better than intimacy. In life nothing can be
        ‘nailed’ - nothing is stable. Life is being created, recreated and causing creation moment by moment.
      </p>
      <p>
        Wittgenstein’s view was that there is an existential gap between life and language:
      </p>
      <p>
        <em>‘What belongs to the essence of the world cannot be expressed by language’.</em>
      </p>
      <p>
        <em>‘It doesn’t strike us at all when we look round us, move about in space, feel our own bodies, etc. etc.
        because there is nothing that contrasts with the form of the world. The, self-evidence of the world, expresses
        itself in the very fact that language can and does only refer to it’.</em>
      </p>
      <p>
        <em>‘You cannot extend experience by thinking any more than you can transmit measles over the phone’.</em>
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="sex.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="variation_unpredictability_and_mathematics.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
