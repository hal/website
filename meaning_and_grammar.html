<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Meaning and grammar
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="meaning_and_grammar">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Meaning and grammar
      </h1>
    </div>
    <div id="content">
      <p>
        ‘Twas brillig, and the slithy toves did gyre and gimble in the wabe; all mimsy were the borogroves, and the
        mome raths outgrabe’.
      </p>
      <p>
        The easiest way to begin to understand grammar and meaning is to glance at the evolution of language.
      </p>
      <p>
        From Chomsky’s work on languages and the identification of a brain area that is crucial to grammar, we know
        that there is a universal ‘grammar’ in all intact humans. Its form has evolved in part from the brain modeling
        of vision.
      </p>
      <p>
        Humans have the best overall vision of any animal particularly in the area of colour where we can detect the
        most subtle hews. The amount of changing light that flows into our brain is overwhelming hence just under a
        half of our brain is involved in seeing. All animals see according to what their brain/eye needs for their
        nature; for the life that they live. What does any animal need the most from its visual brain? It needs distant
        modeling of movement, it needs to move through and anticipate its environment and it needs to identify those
        distant objects that constitute food, a threat and a partner. In <a href="movement.html">movement</a> and
        action we cover vision and movement in some detail <em class="underline">but here we have already found object
        perception and distant movement. We have therefore found the core of nouns and verbs and hence the core of
        grammar.</em>
      </p>
      <p>
        In the <a href="introduction_to_topic_language_and_shared_attention.html">introduction</a> and in the <a href=
        "attention_consciousness_and_self.html">attention</a> topics, we look at the fact that we are wherever our
        point of attention is. Social participation is defined by <a href=
        "intimacy_movement_and_shared_attention.html">shared attention</a>.
      </p>
      <p>
        We examine individual word meanings in more detail later. At the moment the question is how do we move from
        individual word use to grammar. In the physical world as seen, attending to the movement of a known perceived
        object gives us a visual brain model for the grammar of ‘monkey jumping’ or ‘branch bending’. <em>In speech and
        language these can be full sentences.</em> How useful would it be to pass this on to my companion who is
        looking at the ground. The evidence suggests that we would at first have got their attention and then
        physically made signs. A voice representation of ‘monkey jumping’ and ‘branch bending’ would have got attention
        at the same time as sharing meaning. Given the above perspective it would not surprise you to find that
        children’s learning of language runs alongside the development of pointing; mainly by following the gaze of
        other people. Shared attention is crucial.
      </p>
      <p>
        In order to shift from individual word meaning to grammar at least one more word is needed. ‘Hold it’, ‘stop
        moving’ and ‘seeing green’ are all full sentences. The simplest way to generate two word sentences is to put a
        noun and a verb together but this is not the only way as can be seen from the examples just given.
      </p>
      <p>
        <strong>There are different types of words.</strong>
      </p>
      <p>
        From the above picture, we can expect to see two types of words; words that refer to movement and action
        (verbs) and those that refer to objects (nouns). In fact words do divide into two types but not in this way.
        Verbs and nouns are part of one of these two groups.
      </p>
      <p>
        We commonly assume that asking what the word ‘him’ means is the same as asking what the word ‘box’ means. In
        the above ‘nonsense’ poem by Lewis Carroll we can clearly see the two types of word. All the words that you
        know the meaning of are type one words and all the words that you do not know the meaning of – but still ‘feel’
        a sort of meaning – are type two words.
      </p>
      <p>
        Type one words belong to closed classes. New words cannot be added easily. Each class contains a short list of
        words. Grammarians tend to rename and re-order the classes in their quest to examine language but common labels
        would be articles, (a, an, the), pronouns such as ‘him’, prepositions such as ‘by’ and ‘to’ and the auxiliary
        verbs ‘to be’, ‘to have’ and ‘to do’. Words appear in type one because they seem to be essential for the
        structure of language. They behave differently from the general mob of words who sit in open classes where new
        words can be coined. This mob is unruly in the extreme. New ones can be constructed and even used without
        explanation as in the clever, misty, sense/nonsense verse above. They are herded into four fields: nouns, ‘box’
        and ‘confidence’; verbs, ‘eat’ and ‘defend’; adjectives, ‘big’ and ‘obscure’ and adverbs, ‘quietly’ and
        ‘slowly’. Not only are these open classes but many words, with the assistance of some letter changes, can jump
        about between the fields. For example ‘confidence’ is a noun, ‘confident’ an adjective and ‘confidently’ an
        adverb. I do not want to get into grammatical mire here; I only want to point out that there are two quite
        different sources of word meaning. <em class="underline">One source lies within the structure of language
        itself and the other source can be found within the full richness of human experience.</em>
      </p>
      <p>
        Grammatical structures and the meaning of type one words are remarkable things in their own right. Some of them
        are veritable stars of philosophy. Words such as ‘and’ and ‘or’ in logic and the auxiliary verb ‘to be’ in
        general philosophy. In the case of the latter, due to confusing language with reality, a verb that simply acts
        as a place-filler for a process and asserts only that the point of attention is indeed there - ‘it is’ and ‘I
        am’ - has become a major starting point in understanding ourselves within the discipline of philosophy. We look
        at the prepositions in more detail in the movement and action topic.
      </p>
      <p>
        <strong>Meaning and word finding.</strong>
      </p>
      <p>
        An arbitrary set of sounds( a word) is stuck onto shared attention by your human group. You have to stick this
        set of sounds on your shared experience with them. While with them you have to stick it on what <em class=
        "underline">you</em> see, hear and, more difficultly, feel.
      </p>
      <p>
        Meaning is, therefore, a cluster of memories and biological register activity occurring in a moment of time
        which <em class="underline">includes</em> a spoken word articulated by someone else. <em>Mostly the cluster of
        memories will be <a href="episodic_memory.html">episodic</a> in nature.</em> As language develops these
        clusters increasingly exist relatively independently of the words. In a given situation the phrase ‘I am not
        enjoying my coffee’ can be replaced by ‘this drink fails to please’ and by many other phrases often, as in this
        case, with no words in common. Within a language there can be many ways of expressing similar meaning clusters.
        The meaning is not absolutely the same, as superficially and in minor details the emphasis is different.
        Crucially it is the nature of the shared topic which shapes the word set that is used.
      </p>
      <p>
        The separation of meaning clusters from specific words is illustrated by the ‘tip of the tongue’ phenomenon.
        This refers to the situation where you feel a meaning that you want to use in your own thinking, or in
        conversation or writing, and cannot find the word or phrase that you need. Children with speech and language
        problems often show an extreme version of this called word finding difficulties.
      </p>
      <p>
        Remember the picture test in the <a href="speech_and_language.html">speech and language</a> topic (example
        three) used to demonstrate children’s vocabularies. This only tests their grasp of the meaning of the words
        that are spoken to them. They are given the sound shape of the word and from here go to the meaning cluster
        that <em class="underline">they</em> have developed behind it. Some children can demonstrate a good ability on
        this ‘receptive’ test and therefore have a good understanding of the meaning of what they hear. However when
        shown pictures and asked to say the words that express the core meaning they demonstrate a very poor ability.
        They cannot find the words. They have a severe tip of the tongue problem. This will affect their dialogue,
        their inner dialogue (thinking) and their writing but not their understanding of what they hear and read. If
        the problem is not too severe then as the meaning clusters and vocabulary grows, and as the focus on speech
        sounds in reading and spelling develops, the meaning network becomes so rich that a limitation in expression is
        replaced by a more interesting and creative freedom of expression. Some children with a good verbal system can
        say the right thing socially while barely moving through meaning hence raising the question do they know what
        they are talking about. The semantics are there but the meaning clusters are weak.
      </p>
      <p>
        <em class="underline">The difference between semantics and meaning clusters is vital.</em> Semantics is about
        how you move meaningfully through language itself in your talk and thought. It is more orientated towards
        social participation and the establishing of shared understanding. Semantically used words can be thin or rich
        in meaning cluster content. Meaning clusters are <em class="underline">live</em>. They are full of what you
        have felt and sensed. There are many topics on this website that attempt to get to the content of meaning
        clusters. <em class="underline">Throughout we are looking for the nature of life not how we talk about it.</em>
        Computers can be built that do semantics; we are surrounded by them; but they cannot be built to connect with
        meaning clusters and their rich content of live registers. Meaning clusters are life models. We show in many
        topics, but especially in the <a href="variation_unpredictability_and_mathematics.html">variation,
        unpredictability and mathematics</a> topic and the <a href="life_modeling.html">life modeling</a> topic itself
        that these cannot be either mathematical or rational. Neither can algorithm’s escape their precise mathematical
        framework of decidability and computability.
      </p>
      <p>
        Remember when you have read a word within a text and not known its meaning but sort of felt what it was and
        read on. The habit of feeling this word continues when you read it elsewhere and your grasp of its meaning
        seems to grow. Eventually you discover that you got its meaning vaguely right but missed a nuance. Sometimes
        you find that you have been wrong about its meaning. Welcome to the world of the young child learning the
        meaning of words. When children hear words they naturally stick their experience to it. Every grain of their
        ability to experience is deployed. They unconsciously clean stuff away when they realise they are wrong and as
        their language ability grows they can be given pointers to word meanings by adults and other children. Despite
        summary definitions, word meanings are indefinable extending carbuncles of use, sensation, associations,
        feelings and any other register of human experience. The underlying meaning of the words we use is an unruly
        mess of our own making, similar enough to the meaning of the words present in other members of our language
        community to give the impression that we are talking about the same thing, but different enough to be peculiar
        to our own experience.
      </p>
      <p>
        Of course our common experiences also make mutual understanding possible. Oddly, we blame revealed
        misunderstandings on each others ignorance of the true reality rather than differences in our grasp of the
        meaning of words. <em class="underline">It is more appropriate to say that we <strong>feel</strong> meaning
        than it is to say that we think it.</em> We can explore meaning using thought (inner dialogue) but, as we have
        seen, meaning itself is the coalesced experience behind words. We can only feel this.
      </p>
      <p>
        <strong>Language and the troubles we get into.</strong>
      </p>
      <p>
        We know that when we meet people and do not have a common language but share physical space and human
        activities with them communication by actions, gestures, expressions of body and face, intonations and numerous
        other non-verbal means makes for shared experience and intimacy. Elsewhere we call these shared attention life
        models. Indeed we have cultural and research knowledge that when someone says that something is true; ‘no it’s
        fine, really, I like coffee’; but their bodily and facial expressions send the message; ‘I do not like coffee’;
        we believe the non-verbal message. <em>We believe the evidence of what we sense and feel about them as we
        attempt to get to the integrity of their meaning clusters.</em> Research shows that in all situations where
        verbal and non-verbal messages contradict one another we have a preference for believing the non-verbal. Indeed
        we are constantly unconsciously checking other people’s non-verbal actions, movements and gestures in order to
        adapt to them. The matching and mismatching of actions with what people say is a rich source of ambiguity and
        social stress.
      </p>
      <p>
        In addition to the tension between semantics and meaning clusters, their is a tension between semantics and
        verbal memory which is described in the <a href="speech_and_language.html">speech and language</a> topic.
      </p>
      <p>
        <strong>Lets create a new word.</strong>
      </p>
      <p>
        There is no firm ground in language but it is amazingly creative.
      </p>
      <p>
        Let us circumscribe a whole new social emotional state and identify it with a new word. A description of how to
        use the word would run as follows:
      </p>
      <p>
        Someone who you are intimately attached to and are currently sharing a ‘good’ emotional space with says
        something that you take as a criticism. You thought that you were having a good time together and were looking
        after each other. You say nothing so as not to spoil the current pleasure of being together but feel confused,
        disturbed with mixed feelings. Let us call this state mimha.
      </p>
      <p>
        It is easy to make the new word and easy to describe how to use it. You can use it in your own inner dialogue
        right away. The word and its meaning is the more abstract equivalent of the toves we met earlier in Lewis
        Carroll’s poem. ‘Mimha’ feels to have a more secure meaning than the word ‘toves’. Despite the word ‘toves’
        describing what, in theory, is visible, the meaning cluster for mimha seems clearer than the meaning cluster
        for toves. From a grammatical point of view it is toves that seems the weaker of the two. Toves can only be a
        noun as it refers to visible, encounterable objects, while mimha can either be a noun or verb as it can be
        either a state or a process (‘a mimha’ or ‘to mimha’). Symbolic references to feelings, emotions and social
        relationships often have this capacity to be both state and process. Indeed this lends great insecurity to
        their meaning. What are we actually talking about? How real is it? The symbolic system of grammar is built on
        solid modeling ground having evolved in intimate relationship with movement and objects. This can be seen when
        it refers to physical reality but, when it acts as modeling for the social world the slots in its structure can
        be filled with anything. The genius of Lewes Carroll was to refer to the physical using opaque references.
        Similar to the way we see shapes in clouds as we apply our flexible life modeling.
      </p>
      <p>
        <strong>Help! How are we making sense of all this in our encounters with each other? How do we avoid drowning
        in a sea of meaninglessness?</strong>
      </p>
      <p>
        Are we coping by careful reflective thinking, deploying cleverly our vast vocabularies, or are we honing our
        skills in reading various people’s versions of complex social actions right to the level of non-verbal signals?
        Or are we doing a mixture of these? Whatever we are doing we are doing vaguely and imprecisely. We are finding
        a way to stay alive and express our complex human nature in all its variations. We are not following
        mathematically describable rules built on truths or facts. Words do not have mathematical style definitions.
        They use the same <a href="life_modeling.html">life modeling</a> as the rest of our being. We hear words and
        try and work out how people are using them. Children’s brains are built to specialise in this when they are
        young but we are always developing the carbuncles of word use (meaning clusters) from talk, thought and
        sensation, emotion and episodes of experience. While little confusion and ambiguity might surround ‘concrete’
        words such as ‘smile’ or ‘car’ how much is present in the cluttered experience fields for words such as
        ‘extraordinary’, ‘restless’ or ‘ancient’.
      </p>
      <p>
        Wittgenstein:
      </p>
      <p>
        <em>Time and again the attempt is made to use language to limit the world and set it in relief – but it can’t
        be done.</em>
      </p>
      <p>
        We must remember Wittgenstein’s point when we are are being beguiled by semantics. Beautiful language can be
        used as show or to create illusion and an imaginary world. It is not just of interest to philosophers that we
        do not <em class="underline">in practice</em> understand what language is and how it works; or more importantly
        what it is covering up. We only glimpse this when we re-arrange things in such a way that what is hidden
        becomes visible. It turns out that all of us are caught up in a terrible misunderstanding. We concentrate on
        the use and meaning of words under the misapprehension that this constitutes understanding.
      </p>
      <p>
        If you want to explore the topic of meaning further, read the <a href=
        "feeling_and_speech_and_language.html">feeling and speech and language</a> topic. There is also a detailed look
        at semantic/meaning clusters in Place six: Friesian cows, in the <a href="episodic_memory.html">episodic
        memory</a> topic.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="speech_and_language.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="introduction_to_topic_language_and_shared_attention.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
