<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.2.0" />
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <title>
      Episodic memory
    </title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/grids-min.css" type="text/css" />
    <link rel="stylesheet" href="lib/pure/1.0/buttons-min.css" type="text/css" />
  </head>
  <body id="episodic_memory">
    <div id="header">
      <p>
        <a href="index.html">Human Animal Life</a>
      </p>
      <h1>
        Episodic memory
      </h1>
    </div>
    <div id="content">
      <p>
        Once I was listening to a radio interview with an expert in episodic memory. They gave a couple of replies and
        then the interviewer asked ‘yes, but what is it’. The expert said <em class="underline">‘it is
        everything’</em>. I have considerable sympathy with the experts answer.
      </p>
      <p>
        It is in the topic of episodic memory that we can see the great chasm between the mind and the brain. All we
        can do is to try and spend some time in this chasm.
      </p>
      <p>
        Given that a brief account of episodic memory is very difficult, below is an attempt to do that. Before reading
        it, read window one of the <a href="memory.html">memory</a> topic which is about the movement and episodes of a
        small mammal occurring in one early nighttime. Another reason for reading it is that later we refer to it.
      </p>
      <p>
        ….……
      </p>
      <p>
        <strong>A definition of episodic memory</strong>
      </p>
      <p>
        Looked at from the perspective of the human mind, in the human social world stories are made up of episodes
        that, taken together, refer to experience over time. This is true both of an individual, using inner dialogues,
        and of a group using dialogues. Looked at from the perspective of the individual human brain, animal life
        stories are made up of episodes, but the story is lived rather than ‘told’ – referred to. Stories are powerful
        cultural forms because they are basic to our animal nature. In much of ourselves and in all other animals, life
        remains in our body/brain. We can tell our story but we cannot tell our life, we can only live it.
      </p>
      <p>
        The ‘beat’ of the brain is a moment by moment profiling of the state of all of the animal’s biological
        registers and life models. It registers their relative strengths. It is a time profile of them. Slice follows
        slice. It then uses the content of adjacent points of time, and structures them according to both the
        individual variation of each biological register and the time profiles of their moment by moment comparisons
        with other registers I.e. <em class="underline">it structures them into an episode; an animal experience.</em>
      </p>
      <p>
        <em class="underline">Episodes distort time and space.</em> They vary in real time – a packed ten seconds can
        feel as if it went on for much longer: ‘I can recall everything that happened as the car backed into the
        buggy’. Ten minutes can seem like hours: “I waited for you on this uncomfortable bench for a long time”. An
        episode can only exist again if the same pattern <em class="underline">or part of it</em> is experienced again
        i.e. it can only be re-cognised. It is impossible for it to be re-cognised exactly, as there is to much
        variation in encounters (experience). In order to be the repeat of an episode, similarity is required. There
        are degrees of similarity and very close similarity is behind the Deja vu experience. (<em>We explain this
        later in this topic</em>)
      </p>
      <p>
        We used the word ‘structured’ in this definition but we mean <a href="life_modeling.html">life modeling</a>
        which can only be compared to less dynamic structures such as analogy and Wittgenstein's family resemblance
        theory.
      </p>
      <p>
        ….……
      </p>
      <p>
        <strong>A journey through the chasm. Try and look to the mind on one side and your animal biological registers
        and brain on the other.</strong>
      </p>
      <p>
        <em class="underline">Place one: stories</em>
      </p>
      <p>
        With the help of a few conventions it is possible to tell stories in images with no language. Though, in
        reality, the receiver of the story may well be using a great deal of inner dialogue at least at the level of
        the meaning clusters of words. (<a href="meaning_and_grammar.html">meaning and grammar</a>). Even so they would
        need to recognise the word-presented images in a way compatible with their stored images - have you stuck the
        words ‘bush’, ‘party’ and ‘sad’ on <em class="underline">similar enough</em> encounters to the image makers?
        Even for something as visible as a dog are your images and films of ‘dog’ the same as mine? They cannot be the
        same. They can only be similar enough. For more about the relationship between episodic memories and the
        meaning clusters behind words read place six below.
      </p>
      <p>
        Episodes begin and fill stories. There is nothing more powerful in a classroom of children; from the poor
        concentration of three year olds to the embarrassment and questioning of teenagers; than the starting off of,
        what might turn out to be, a story. I have seen this thousands of times, but the power of just beginning what
        might be a story always leaves me awe struck. Clearly a story does not have to be announced. It is recognised
        by early clues in initial speaking. Needless to say the most slippery challenging child to have in a class is
        the good story teller who likes to be heard by the whole class and vies with the teacher for the right to
        address them.
      </p>
      <p>
        Stories have a full range of feelings, emotions and senses and every type of life model you can think of from
        intimacy to belief, movement and episodic memory.
      </p>
      <p>
        <em class="underline">Place two: returning</em>
      </p>
      <p>
        Episodic memory is used by brains to help to identify specific places. I have been here before. <em>This is my
        den.</em> Note that there are two sources of information about a specific place. Have you ever had that feeling
        when returning to a place that is packed with memories that the place might not be there? The house; to take a
        common example; will have disappeared and the only trace will be a hole where it was. We tend to get this
        feeling as we walk towards it before it comes into sight. This is the integration point of the two processes
        that identify ‘same place’.
      </p>
      <p>
        We have integrated <a href="movement.html">movement</a> models of physical spaces. In window one in the
        movement topic it was these that physically took our animal to the slugs and these that brought it back to its
        den. It is now taking us back to a house, set in a distant location, that we once lived in. We are full of
        triggered and recalled episodic memories as we approach. These are battling with movement modeling for our
        focussed attention. Movement modeling is being thrown out of kilter. It is operating, and trying to update,
        very old movement models and finding that objects have changed – not forgetting that ‘object’ includes the type
        of ground, trees, bushes, specific makes of car and clutter as well as buildings, front garden walls and gates.
        Many of these objects have strong episodic memories: in both their presence and their absence. Our mind may be
        noting some changes but more fundamentally our episodic memory and movement models are doing somersaults.
        Emotional registers will be part of recognised and recalled episodes as well as featuring in the episodes being
        made as you walk towards the house.
      </p>
      <p>
        As you move towards it, within the movement model the house is just an area of space, at best a general sketchy
        house-like object. At the same time your brain and mind are full of episodic memories of the house. The split
        between totally different processes is highlighted and exposed. Your natural conflicts are in and out of
        focussed attention at an uncomfortable rate. You have the feeling that the remembered house may not be in the
        physical space that you are going to, because, in a sense, it isn’t. Your mind may identify and name this
        feeling. The feeling will increase to the extent that the movement model has to be updated. It may break down
        completely leaving you unable to find the house.
      </p>
      <p>
        <em class="underline">Place three: Deja vu</em>
      </p>
      <p>
        Déjà vu is a feeling that arises at the boundary between ‘known places’ and ‘new places made up of familiar
        registers’. The detail of the episodic experience (often also including related movement models) that registers
        what your inner dialogue believes to be a new place can be so consistent with an episodic memory of a ‘known’
        place that your brain considers it to <em class="underline">be</em> that place. You are revisiting it. Your
        inner dialogue and the rest of your brain are at odds with each other. Either could be right. You might switch
        to belief in the thought that you have been to this place before. Appropriate ‘research’ may satisfy both
        yourself and those in your social world that this is ‘true’. You may treat it as an unexplained mystery or in
        the teeth of the impossibility of you having been there before you may conclude that you must have been there
        in a previous life. <em>In the world of <a href="believing_and_knowing.html">belief</a> all and any possible
        thought can be believed in. You only need to commit to it (switch it on) – see earlier.</em>
      </p>
      <p>
        <em class="underline">Place four: ouzo</em>
      </p>
      <p>
        The gap between our episodic experience and the overlay of the mind is captured in this personal story.
      </p>
      <p>
        In my early twenties I traveled with a friend around the Balkans, Turkey and Greece. Traveling on a train
        through Bulgaria we were pressed into sharing ouzo with a group of local people. <em>I am using the Greek name
        for the common aniseed tasting alcoholic drink typical of the region.</em> We had a couple of similar
        experiences with a group of Turkish soldiers that we met on a beach. This was followed by drinking sessions
        with Greeks that we met on a ferry. Ouzo was always the drink of choice as our new friends were proud of their
        local drink. I brought a bottle back with me. A few months after returning, I tried some ouzo and I couldn’t
        drink it. A kind of shudder stopped me. Over the years I came across it again. I always tried it but could
        never drink it.
      </p>
      <p>
        Later still I had some food that gave me the same shudder and I could not finish it. &nbsp;It took a long time
        for my mind to link all these episodes together and to realise that the emotion of disgust energised my
        body/brain to develop a highly tuned substance detection ability to identify the chemical profile of both
        aniseed, and fennel, in minute quantities within food and drink. My ability to do this was worthy of comment.
        Needless to say my detection of it was followed by the inability to eat or drink the object (also the work of
        the emotion we refer to as disgust). It took my mind many years to work this out. My body/brain first
        encountered aniseed in some sweets as a child but, much as with the bereaved sheep, the episodic profile for
        aniseed had been changed from a co-presence with sugar to a co-presence with alcohol. Given the sequence of
        episodes in the Balkans, aniseed, and by extension fennel, became unwelcome to my body/brain. My mind has now
        spent decades trying to change its decision by setting up episodes associating aniseed and fennel with sugar.
        No success yet.
      </p>
      <p>
        <em class="underline">Place five: truth and telling a good story</em>
      </p>
      <p>
        When telling personal stories it is easy for semantic memory, cultural story, grammatical cliches and
        constructed imagery to infect our ‘memory’. Indeed if we are true to the episodes that we can access we will be
        aware that we are distorting them. Instead of telling ourselves that we have not been accurate we may commit to
        the version we have told ourselves and/or others. If we believe what we have ‘said’, then the truth in
        ourselves will be obscured and increasingly hard to access. Others will believe things that are not true. It is
        of course possible to construct personal stories, parts of stories or details of stories that are not true. If
        tagged with the ‘true’ label others will believe on trust. <em class="underline">Trust is crucial for social
        cohesion.</em>
      </p>
      <p>
        The ability of a story to make a complex world look simple, its plausibility and its usefulness also endear it
        to others. The individual or group that constructed the story can even come to believe it themselves. ‘Truth’
        as a generality is largely about belief and trust and very little to do with a good and thorough grounding in
        our personal encounters with the physical world. This includes the physical reality of other people’s lives.
      </p>
      <p>
        We are aware that we live in a world of story danger and the core meaning cluster for story includes ‘not true
        until told it is and, even then, check it’. We tend to say things such as ‘this is what happened to me’ if we
        want to share experience, or make statements. They gave me an account of what happened/they told me a story
        about what happened. They told me what happened <em class="underline">means</em> they told you a story tagged
        as true.
      </p>
      <p>
        <em class="underline">Place six: Friesian cow</em>
      </p>
      <p>
        For me, this phrase has a lot of episodic memories behind it. If I keep it in my mind I can find a number of
        episodes strong enough to make it into focussed attention.
      </p>
      <p>
        I first find visual images of being in a farm on holiday with my parents in the West Country. I would have been
        about eight years old.
      </p>
      <p>
        <em>I am thinking this memory because I am referring to it in language. I cannot show you the images or plug
        you into my sadness. I am thinking only in order to write thoughts that describe both the images (still or
        moving) and the feelings that arise from episodic memory. When you read my writing of ‘farm holiday’ then you
        construct a model using primarily the visual content of your meaning cluster for that phrase. If you paused and
        gave the task slightly more time and focus then you may also recall experiences of your own stored in episodic
        memory. For example, when reading the word ‘farm’ the meaning cluster images that you use would likely consist
        of sketchy models and a sense of nearby meaning clusters and their attached spoken words; all this taking
        milliseconds. From here you could fill out the meaning with ‘examples’ from episodic memory. Any episode that
        you find would be a record of a real event containing pictures taken by your brain. These images are not
        models. They even move slightly, like very short films, because they capture the body movement; however small;
        at the time. Sometimes the episode they are from can be recalled. For modernised humans our semantic/meaning
        cluster memory has access to a rich store of photographic images and films. This is a new integrated system
        that has to be built by the brain. It is a new system because we distinguish between; tag differently; images
        and episodes that we have seen on film and those that are part of our personal experience. Humans in
        traditional cultures have to learn the conventions for engaging with pictures and films.</em>
      </p>
      <p>
        From the farm episode I have images of me going out of the farm door and across a small courtyard to see the
        farmer’s bull. The scene then shifts to a small, <em>smelly</em>, gloomy ‘shed’ with some straw over a
        <em>beaten earth</em> floor. The farmer is standing next to me as we both look at a <em>large</em> brown bull.
        I can remember him saying things that conveyed the information that it was a friendly animal. He explained what
        the ring through its nose was for and told me that Friesian bulls were the most dangerous and the hardest to
        handle. I have no memory for the actual words that he used or of his voice, though these could have potentially
        been components of the episode.
      </p>
      <p>
        Actually, as you have spotted, it is in fact a memory of two episodes, as I only have a memory for going into
        the courtyard and a separate memory of standing in the ‘shed’. These are stored ‘near’ to one another. As with
        case of remembering how to make a box out of a piece of paper (window four in the <a href=
        "memory.html">memory</a> topic), the first episode that appeared was the last in the sequence; thereby showing
        that individual episodes are distinct even when they are ‘close’ to one another. This is because they are not a
        story but records of differing physical and social encounters. They are episodes, each with its own emotional,
        movement and sensory profile as well as being set in distinct physical and social environments. In the first
        episode it is light and in the second it is dark. The sounds are different in each case. In the first episode I
        am walking and in the second I am standing. The emotional mix is different in each episode. Episodes are very
        complex and detailed animal moments.
      </p>
      <p>
        In the above paragraph the words in italics show evidence of semantic intrusion into episodic recall. What is
        happening in the case of the word ‘me’, is easy for anyone to spot. The other intrusions depend on my personal
        judgment. Of course, all or any part of this memory could be a complete fabrication. It then ceases to be a
        memory and becomes a story. We are used to considerable individual and social uncertainty around memory, true
        story and made up story. At this point in our observations we are looking at the basic slip and slide points
        where episodic memory becomes infected by semantic/meaning cluster memory and constructed episodes. We invite
        this infection as soon as we talk about and recall personal experiences from episodic memory.
      </p>
      <p>
        <em class="underline">The episodic memory of any animal cannot have an image of itself because it never sees
        itself.</em> It is therefore impossible to have an episodic memory that includes an image of <em>‘me’</em>
        doing something. My brain cannot see an image of me when it looks out of my eyes. It can see only parts of me.
        The length of time that it takes for infants to realise that they are looking at themselves in a mirror
        indicates that this is a complex and very separate piece of brain modeling. Confirmation that this ability has
        been constructed only comes when the child makes faces or touches part of their head in order to see the action
        mirrored. It remains unclear which other animals are able to recognise themselves mirrored. The problem is that
        a lot of the research has been done with social animals who naturally take a keen interest in each other, in
        the same way that infants do when they first encounter their mirrored image as another baby that appears and
        disappears.
      </p>
      <p>
        If you see yourself in a memory then you are compounding a memory of episodes with constructed features. Film
        and images of yourself at different ages can easily be used and animated by your brain to place yourself in
        personally historic locations and add shots of you doing things alongside shots of what you are looking at. In
        remembering the farm episodes I drifted, and once or twice imagined that I could see myself there and noted
        that I was animating a remembered picture of myself as a seven year old. This is going to be an increasing
        problem as we keep many more films and images of ourselves.
      </p>
      <p>
        The remaining words in italics are <em>‘smelly’</em>, <em>‘beaten earth’</em> and <em>‘large’</em>. In my
        thinking and writing all of them began as fillers taken from story forms – constructed from semantic memory and
        episodes from anywhere else, including the new ‘special’ area of film and picture. It is time consuming and
        mental hard work to build thoughts word by word (meaning cluster to meaning cluster). It would slow down
        conversations and lead to more of them breaking down. Our brain uses as many standard phrases; ways of thinking
        and talking about things; as it can. It does this at the levels of both verbal (word sound) memory and
        semantic/meaning cluster memory.
      </p>
      <p>
        There is also a biological social tendency to use common language; grammar, vocabulary and topics; in order to
        establish and strengthen the register of human-animal friend. We talk to each other rather than picking ticks
        and fleas off one another. It is therefore serious when conversations break down and starting and ending
        conversations is ‘tricky’ in human encounters. Our brain uses quick standard ways in both dialogues with others
        and in inner dialogues.
      </p>
      <p>
        When we think about anything our brain will be inclined to use known and readily accessible phrasing. My brain
        was doing just that when it focussed on the episodic records of my bull encounter on the Devon farm. I had to
        make an effort to check carefully whether or not the words in italics actually described content from the
        memory or whether they were language infections. I am not one of those people who can recall smells so my
        description of the bull shed as ‘smelly’ is fictitious. It is plausible but I am not writing a story but trying
        to describe to myself and others an episodic memory. I remain unsure about ‘beaten earth’. I can recall a light
        distribution of straw over a darkish floor but cannot get beyond this. The word ‘large’ I could have removed
        very quickly as the bull was smaller in height than I expected but much bigger in girth. The word ‘large’ in
        this context would have been misleading and would have distorted my, and therefore your, image of the bull, as
        the use of words brings in their meaning content. This includes images. In this case to use the word large
        would change the shape of the bull and mislead myself and you as to its height.
      </p>
      <p>
        In order for me to remember the encounter with the bull, high emotional registers would be required. I can
        remember being very interested. <em>Some would use the word excited here.</em> Interest is an emotion. I can
        also remember being happy at the point that I was standing next to the farmer looking at the bull.
      </p>
      <p>
        Though the noun phrase ‘Friesian cow’ was the starting point we came first to an encounter with a bull that was
        not a Friesian. The farmer told me that Friesian bulls were the most dangerous. I then spent many years hoping
        to meet them. The only success I can remember was when I saw them in a field by a farm, where they were kept
        for their semen for use in artificial insemination. I was about twelve.
      </p>
      <div class="pure-g" id="footermenu">
        <div class="pure-u-1-3 previous">
          <a class="pure-button" href="memory.html">Previous</a>
        </div>
        <div class="pure-u-1-3 home">
          <a class="pure-button" href="index.html">Home</a>
        </div>
        <div class="pure-u-1-3 next">
          <a class="pure-button" href="attention_consciousness_and_self.html">Next</a>
        </div>
      </div>
      <div id="footer">
        <p>
          Copyright © 2017 Robert Daines
        </p>
        <p>
          Hosted by <a href="https://www.webarchitects.coop/">Webarchitects Co-operative</a>
        </p>
      </div>
    </div>
  </body>
</html>
